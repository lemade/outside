source 'https://rubygems.org'

ruby '~> 3.2.4'

gem 'rails', '~> 7.1.0'

gem 'active_storage_validations'
gem 'bcrypt', '~> 3.1.7'
gem 'best_in_place', git: "https://github.com/mmotherwell/best_in_place"
gem 'bootsnap', require: false
gem 'delayed_job_active_record'
gem 'delayed_job_recurring'
gem 'dotenv-rails'
gem 'image_processing', '~> 1.2'
gem 'kramdown'
gem 'mini_magick'
gem 'mysql2'
gem 'observer'
gem 'puma', '>= 5.0'
gem 'responders'
gem 'rucaptcha', '~> 3.2.0'
gem 'slim-rails'
gem 'webpacker', '~> 5.0'
gem 'will_paginate'

group :development do
  gem 'brakeman', require: false
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'rubocop', require: false
  gem 'rubocop-rails', require: false
  gem 'web-console', '>= 4.1.0'
end

group :development, :test do
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'rspec-its'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
end

group :test do
  gem 'capybara', '>= 3.26'
  gem 'database_cleaner'
  gem 'selenium-webdriver', '>= 4.0.0.rc1'
  gem 'simplecov', require: false
  gem 'webdrivers', "= 5.3.0"
end
