# Outside the Box - Zeitschrift für feministische Gesellschaftskritik

## Development

### Requirements

* checkout the ruby version we currently use within the Gemfile and set up - e.g. with 
    [rbenv](https://wiki.archlinux.org/index.php/Rbenv) 
    and the [ruby-build plugin](https://aur.archlinux.org/packages/ruby-build/)
* install [Ruby on Rails](https://wiki.archlinux.org/index.php/Ruby_on_Rails#Installation) 
* set up mysql on your host machine - e.g. 
    [mariadb](https://wiki.archlinux.org/index.php/MariaDB#Installation)

### Setup

    bundle install
    bundle exec rake db:create db:migrate

### Run

    bundle exec rails s    

### Local Mails

We use delayed jobs to deliver mails asynchronously in the background. 

On your host machine you can start job proceeding with

    rake jobs:start  
    
You can use mailcatcher to view mails. Set up with

    gem install mailcatcher
    mailcatcher (start mailcatcher)
    
and visit [localhost](http://localhost:1080) on port 1080 

### Testing

#### Setup
We use the chrome driver to run JS test with Selenium. You can run chrome within a 
docker container:

    docker run -d -p 4444:4444 selenium/standalone-chrome:latest
    
Then run your specs 

    bundle exec rspec

## Maintenance

### Update gems

    bundle update <dependency>

### Rails console

    bundle exec rails c

### Connecting to database

    mysql -h mysql -uroot -proot development

### Get mysql dump into database

    mysql -h mysql -uroot -proot development < ~/path/to/my_dump.sql

### Code Analysis

    rubocop -DRa
    bundle exec app brakeman

## Deployment

    BUNDLE_GEMFILE=Gemfile.capistrano bundle install
    BUNDLE_GEMFILE=Gemfile.capistrano bundle exec cap staging deploy
