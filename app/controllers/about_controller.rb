# frozen_string_literal: true

class AboutController < PublicController
  # GET /about
  def show
    @about = About.new
    @links = Link.all
    @title = t('labels.about.public')
  end
end
