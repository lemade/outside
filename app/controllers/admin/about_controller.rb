# frozen_string_literal: true

module Admin
  class AboutController < Admin::ApplicationController
    # GET /admin/about
    def show
      @about = About.new
      @title = t('labels.about.menu')
    end

    # GET /admin/about/edit
    def edit
      @about = About.new
      @title = t('labels.about.menu')
      render :form
    end

    # PUT /admin/about
    def update
      @about = About.new

      if @about.update_attributes(params[:about]) # rubocop:disable Rails/ActiveRecordAliases
        flash[:notice] = t('messages.success.save')
        redirect_to admin_about_url
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.about.menu')
        render :form
      end
    end
  end
end
