# frozen_string_literal: true

module Admin
  class ApplicationController < ::ApplicationController
    layout 'admin'

    before_action :authenticate
    before_action :authorize

    def authorize
      return if current_user.admin?

      flash[:alert] = t('messages.authentication.error')
      redirect_to admin_url
    end

    def authenticate
      redirect_to(admin_login_url) unless current_user
    end
  end
end
