# frozen_string_literal: true

module Admin
  class AttachmentsController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_attachment_set, only: %i[create destroy]

    # POST /admin/attachment_sets/:attachment_set_id/attachments
    def create
      if @attachment_set.attachments.create(attachment_params)
        flash[:notice] = t('messages.attachment.success.save')
      else
        flash[:alert] = t('messages.attachment.error.save')
      end

      redirect_back(fallback_location: admin_path)
    end

    # DELETE /admin/attachment_sets/:attachment_id/attachments/:id
    def destroy
      if @attachment_set.attachments.find(params[:id]).destroy
        flash[:notice] = t('messages.attachment.success.delete')
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_back(fallback_location: admin_path)
    end

    private

    def attachment_params
      params.require(:attachment)
            .permit(:picture, :caption, :picture_delete, attachment_set_attributes: [:id])
    end

    def find_attachment_set
      @attachment_set = AttachmentSet.find(params[:attachment_set_id])
    end
  end
end
