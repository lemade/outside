# frozen_string_literal: true

module Admin
  class AuthorsController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_author, only: %i[show edit update destroy]

    # GET /admin/authors
    def index
      @authors = Author.search(params).page(params[:page]).limit(15).ordered
      @title = t('labels.author.index')
    end

    # GET /admin/authors/:id
    def show
      @posts = @author.posts.order(:issue_id).page(params[:page]).limit(8)
      @papers = @author.papers.page(params[:page]).limit(8).ordered
      @title = @author.name
    end

    # GET /admin/authors/new
    def new
      @author = Author.new
      @title = t('labels.author.menu')
      @subtitle = t('labels.author.new')
      render :form
    end

    # POST /admin/authors
    def create
      @author = Author.new(author_params)

      if @author.save
        flash[:notice] = t('messages.author.success.save', name: @author.name)
        redirect_to admin_author_url(@author)
      else
        flash.now[:alert] = t('messages.author.error.save')
        @title = t('labels.author.menu')
        @subtitle = t('labels.author.new')
        render :form
      end
    end

    # GET /admin/authors/:id/edit
    def edit
      @title = t('labels.author.menu')
      @subtitle = @author.name
      render :form
    end

    # PUT /admin/authors/:id
    def update
      if @author.update(author_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_author_url(@author)
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.author.menu')
        @subtitle = @author.name
        render :form
      end
    end

    # DELETE /admin/authors/:id
    def destroy
      if @author.destroy
        flash[:notice] = t('messages.author.success.delete', name: @author.name)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_authors_url
    end

    private

    def author_params
      params.require(:author).permit(:name, :info)
    end

    def find_author
      @author = Author.find(params[:id])
    end
  end
end
