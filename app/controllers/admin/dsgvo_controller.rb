# frozen_string_literal: true

module Admin
  class DsgvoController < Admin::ApplicationController
    # GET /admin/dsgvo
    def show
      @dsgvo = Dsgvo.new
      @title = t('labels.dsgvo.menu')
    end

    # GET /admin/dsgvo/edit
    def edit
      @dsgvo = Dsgvo.new
      @title = @dsgvo.title
      render :form
    end

    # PUT /admin/dsgvo
    def update
      @dsgvo = Dsgvo.new

      if @dsgvo.update_attributes(params[:dsgvo]) # rubocop:disable Rails/ActiveRecordAliases
        flash[:notice] = t('messages.success.save')
        redirect_to admin_dsgvo_path
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.dsgvo.menu')
        render :form
      end
    end
  end
end
