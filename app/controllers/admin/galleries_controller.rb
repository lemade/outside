# frozen_string_literal: true

module Admin
  class GalleriesController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_post, only: %i[create destroy]

    # POST /admin/posts/:post_id/gallery
    def create
      if @post.create_gallery
        flash[:notice] = t('messages.gallery.success.add')
      else
        flash[:alert] = t('messages.gallery.error.add')
      end

      redirect_to admin_issue_post_url(@post.issue_id, @post)
    end

    # DELETE /admin/posts/:post_id/gallery
    def destroy
      if @post.gallery.try(:destroy)
        flash[:notice] = t('messages.gallery.success.delete')
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_issue_post_url(@post.issue_id, @post)
    end

    private

    def find_post
      @post = Post.find(params[:post_id])
    end
  end
end
