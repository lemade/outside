# frozen_string_literal: true

module Admin
  class ImpressumController < Admin::ApplicationController
    # GET /admin/impressum
    def show
      @impressum = Impressum.new
      @title = t('labels.impressum.menu')
    end

    # GET /admin/impressum/edit
    def edit
      @impressum = Impressum.new
      @title = @impressum.title
      render :form
    end

    # PUT /admin/impressum
    def update
      @impressum = Impressum.new

      if @impressum.update_attributes(params[:impressum]) # rubocop:disable Rails/ActiveRecordAliases
        flash[:notice] = t('messages.success.save')
        redirect_to admin_impressum_url
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.impressum.menu')
        render :form
      end
    end
  end
end
