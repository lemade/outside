# frozen_string_literal: true

module Admin
  class IssuesController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_issue, only: %i[edit update destroy]

    # GET /admin/issues
    def index
      @issues = Issue.all
      @title = t('labels.issue.index')
    end

    # GET /admin/issues/new
    def new
      @issue = Issue.new
      @title = t('labels.issue.new')
      render :form
    end

    # POST /admin/issues
    def create
      @issue = Issue.new(issue_params)

      if @issue.save
        flash[:notice] = t('messages.issue.success.save', title: @issue.title)
        redirect_to admin_issues_url
      else
        flash.now[:alert] = t('messages.issue.error.save')
        render :form
      end
    end

    # GET /admin/issues/:id
    def edit
      @title = t('labels.issue.menu')
      @subtitle = @issue.title
      render :form
    end

    # PUT /admin/issues/:id
    def update
      if @issue.update(issue_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_issues_path
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.issue.menu')
        @subtitle = @issue.title
        render :form
      end
    end

    # DELETE /admin/issues/:id
    def destroy
      if @issue.destroy
        flash[:notice] = t('messages.issue.success.delete', title: @issue.title)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_issues_url
    end

    private

    def issue_params
      params.require(:issue).permit(:title, :picture, :published)
    end

    def find_issue
      @issue = Issue.find(params[:id])
    end
  end
end
