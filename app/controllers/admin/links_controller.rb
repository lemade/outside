# frozen_string_literal: true

module Admin
  class LinksController < Admin::ApplicationController
    before_action :find_link, only: %i[edit update destroy]

    # GET /admin/links
    def index
      @links = Link.search(params).page(params[:page]).limit(15)
      @title = t('labels.link.index')
    end

    # GET /admin/links/new
    def new
      @link = Link.new
      @title = t('labels.link.menu')
      @subtitle = t('labels.link.new')
      render :form
    end

    # POST /admin/links
    def create
      @link = Link.new(link_params)

      if @link.save
        flash[:notice] = t('messages.link.success.save', name: @link.name)
        redirect_to admin_links_url
      else
        flash.now[:alert] = t('messages.link.error.save')
        @title = t('labels.link.menu')
        @subtitle = t('labels.link.new')
        render :form
      end
    end

    # GET /admin/links/:id/edit
    def edit
      @title = t('labels.link.menu')
      @subtitle = @link.name
      render :form
    end

    # PUT /admin/links/:id
    def update
      if @link.update(link_params)
        flash.now[:notice] = t('messages.success.save')
        redirect_to admin_links_url
      else
        flash[:alert] = t('messages.error.save')
        @title = t('labels.link.menu')
        @subtitle = @link.name
        render :form
      end
    end

    # DELETE /admin/links/:id
    def destroy
      if @link.destroy
        flash[:notice] = t('messages.link.success.delete', name: @link.name)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_links_url
    end

    private

    def link_params
      params.require(:link).permit(:name, :url)
    end

    def find_link
      @link = Link.find(params[:id])
    end
  end
end
