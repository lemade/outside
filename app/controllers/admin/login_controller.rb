# frozen_string_literal: true

module Admin
  class LoginController < Admin::ApplicationController
    layout 'aether'

    skip_before_action :authorize
    skip_before_action :authenticate, except: [:logout]
    before_action      :forward,      except: [:logout]

    def login
      @username = params[:username]
      user = User.find_by(username: @username)

      unless user.try(:authenticate, params[:password])
        flash.now[:alert] = t('messages.login.error.login')
        render(:form) && return
      end

      unless user.refresh_login_hash!
        flash.now[:alert] = t('messages.login.error.refresh')
        render(:form) && return
      end

      session[:login_hash] = user.login_hash
      flash[:notice] = t('messages.login.success.login')
      redirect_to admin_url
    end

    def logout
      @username = current_user.username
      session[:login_hash] = nil
      redirect_to root_url
    end

    private

    def forward
      redirect_to(admin_url) if current_user
    end
  end
end
