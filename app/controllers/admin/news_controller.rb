# frozen_string_literal: true

module Admin
  class NewsController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_news, only: %i[show edit update destroy]

    # GET /admin/news
    def index
      @news = News.search(params, current_user).page(params[:page]).limit(15).ordered
      @title = t('labels.news.index')
    end

    # GET /admin/news/:id
    def show
      @title = t('labels.news.menu')
    end

    # GET /admin/news/new
    def new
      @news = News.new
      @title = t('labels.news.menu')
      @subtitle = t('labels.news.new')
      render :form
    end

    # POST /admin/news
    def create
      @news = News.new(news_params)
      @news.attachment_set = AttachmentSet.find(params[:attachment_set_id])
      @news.user = current_user

      if @news.save
        flash[:notice] = t('messages.news.success.save', title: @news.title)
        redirect_to admin_news_path(@news)
      else
        flash.now[:alert] = t('messages.news.error.save')
        @title = t('labels.news.menu')
        @subtitle = t('labels.news.new')
        render :form
      end
    end

    # GET /admin/news/:id/edit
    def edit
      @title = t('labels.news.menu')
      @subtitle = @news.title
      render :form
    end

    # PUT /admin/news/:id
    def update
      @news.user = current_user

      if @news.update(news_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_news_path(@news)
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.news.menu')
        @subtitle = @news.title
        render :form
      end
    end

    # DELETE /admin/news/:id
    def destroy
      if @news.destroy
        flash[:notice] = t('messages.news.success.delete', title: @news.title)
      else
        flash[:alert] = t('messages.delete.error')
      end

      redirect_to admin_news_index_path
    end

    private

    def news_params
      params.require(:news).permit(:tag, :title, :subtitle, :info, :body, :published,
                                   :on_stage, :on_stage_from, :on_stage_until)
    end

    def find_news
      @news = News.find(params[:id])
    end
  end
end
