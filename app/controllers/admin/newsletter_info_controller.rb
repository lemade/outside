# frozen_string_literal: true

module Admin
  class NewsletterInfoController < Admin::ApplicationController
    # GET /admin/newsletter_info/edit
    def edit
      @newsletter_info = NewsletterInfo.new
      @title = @newsletter_info.title
      render :form
    end

    # PATCH /admin/newsletter_info/:id
    def update
      @newsletter_info = NewsletterInfo.new

      if @newsletter_info.update_attributes(params[:newsletter_info]) # rubocop:disable Rails/ActiveRecordAliases
        flash[:notice] = t('messages.success.save')
        redirect_to admin_subscribers_path
      else
        flash.now[:alert] = t('messages.error.save')
        @title = @newsletter_info.title
        render :form
      end
    end
  end
end
