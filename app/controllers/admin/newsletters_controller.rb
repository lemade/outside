# frozen_string_literal: true

module Admin
  class NewslettersController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_newsletter, only: %i[show edit update sendletter destroy]

    # GET /admin/newsletters
    def index
      @unpublished = Newsletter.unpublished
      @published = Newsletter.published.page(params[:page]).limit(15)
      @title = t('labels.newsletter.index')
    end

    # GET /admin/newsletters/:id
    def show
      @title = t('labels.newsletter.menu')
    end

    # GET /admin/newsletters/new
    def new
      @newsletter = Newsletter.new
      @title = t('labels.newsletter.menu')
      @subtitle = t('labels.newsletter.new')
      render :form
    end

    # POST /admin/newsletters
    def create
      @newsletter = Newsletter.new(newsletter_params)
      @newsletter.user = current_user

      if @newsletter.save
        flash[:notice] = t('messages.newsletter.success.save', title: @newsletter.title)
        redirect_to admin_newsletter_url(@newsletter)
      else
        flash.now[:alert] = t('messages.newsletter.error.save')
        @title = t('labels.newsletter.menu')
        @subtitle = t('labels.newsletter.new')
        render :form
      end
    end

    # GET /admin/newsletters/:id/edit
    def edit
      @title = t('labels.newsletter.menu')
      @subtitle = @newsletter.title
      render :form
    end

    # PUT /admin/newsletters/:id
    def update
      @newsletter.user = current_user

      if @newsletter.update(newsletter_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_newsletter_url(@newsletter)
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.newsletter.menu')
        @subtitle = @newsletter.title
        render :form
      end
    end

    # POST /admin/newsletters/:id/sendletter
    def sendletter
      if @newsletter.send_to(Subscriber.confirmed)
        flash[:notice] = t('messages.newsletter.success.published', title: @newsletter.title)
      else
        flash[:alert] = t('messages.newsletter.error.published')
      end

      redirect_to admin_newsletters_url
    end

    # DELETE /admin/newsletters/:id
    def destroy
      if @newsletter.destroy
        flash[:notice] = t('messages.newsletter.success.delete', title: @newsletter.title)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_newsletters_url
    end

    private

    def newsletter_params
      params.require(:newsletter).permit(:title, :body, :published)
    end

    def find_newsletter
      @newsletter = Newsletter.find(params[:id])
    end
  end
end
