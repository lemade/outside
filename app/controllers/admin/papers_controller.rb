# frozen_string_literal: true

module Admin
  class PapersController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_paper,   only: %i[show edit update destroy]
    before_action :find_authors, only: %i[new edit]

    # GET /admin/papers
    def index
      @papers = Paper.search(params, current_user).page(params[:page]).limit(15).ordered
      @title = t('labels.paper.index')
    end

    # GET /admin/papers/:id
    def show
      @title = t('labels.paper.menu')
    end

    # GET /admin/papers/new
    def new
      @paper = Paper.new
      @title = t('labels.paper.menu')
      @subtitle = t('labels.paper.new')
      render :form
    end

    # POST /admin/papers
    def create
      @paper = Paper.new(paper_params)
      @paper.attachment_set = AttachmentSet.find(params[:attachment_set_id])
      @paper.authors = Author.from_s(params[:authors]) if params[:authors]
      @paper.user = current_user

      if @paper.save
        flash[:notice] = t('messages.paper.success.save', title: @paper.title)
        redirect_to admin_paper_url(@paper)
      else
        flash.now[:alert] = t('messages.paper.error.save')
        @title = t('labels.paper.menu')
        @subtitle = t('labels.paper.new')
        render :form
      end
    end

    # GET /admin/papers/:id/edit
    def edit
      @title = t('labels.paper.menu')
      @subtitle = @paper.title
      render :form
    end

    # PUT /admin/papers/:id
    def update
      @paper.authors = Author.from_s(params[:authors]) if params[:authors]
      @paper.user = current_user

      if @paper.update(paper_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_paper_url(@paper)
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.paper.menu')
        @subtitle = @paper.title
        render :form
      end
    end

    # DELETE /admin/papers/:id
    def destroy
      if @paper.destroy
        flash[:notice] = t('messages.paper.success.delete', title: @paper.title)
      else
        flash[:alert] = t('messages.delete.error')
      end

      redirect_to admin_papers_url
    end

    private

    def paper_params
      params.require(:paper).permit(:tag, :title, :subtitle, :body, :published)
    end

    def find_paper
      @paper = Paper.find(params[:id])
    end

    def find_authors
      @authors = Author.pluck(:name)
    end
  end
end
