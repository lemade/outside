# frozen_string_literal: true

module Admin
  class PostsController < Admin::ApplicationController
    skip_before_action :authorize
    before_action :find_issue,   only: %i[index show new create edit update]
    before_action :find_post,    only: %i[show edit update destroy]
    before_action :find_authors, only: %i[new edit]

    # GET /admin/issues/:issue_id/posts
    def index
      @title = @issue.title
      @subtitle = t('labels.post.index')
    end

    # GET /admin/:issues/:issue_id/posts/:id
    def show
      @title = @issue.title
    end

    # GET /admin/issues/:issue_id/posts/new
    def new
      @post = Post.new
      @title = @issue.title
      @subtitle = t('labels.post.new')
      render :form
    end

    # POST /admin/issues/:issue_id/posts
    def create
      @post = @issue.posts.build(post_params)
      @post.attachment_set = AttachmentSet.find(params[:attachment_set_id])
      @post.authors = Author.from_s(params[:authors]) if params[:authors]
      @post.user = current_user

      if @post.save
        flash[:notice] = t('messages.post.success.save', title: @post.title)
        redirect_to admin_issue_post_url(@issue, @post)
      else
        flash.now[:alert] = t('messages.post.error.save')
        @title = @issue.title
        @subtitle = t('labels.post.new')
        render :form
      end
    end

    # GET /admin/issues/:issue_id/posts/:id
    def edit
      @title = @issue.title
      @subtitle = @post.title
      render :form
    end

    # PUT /admin/issues/:issue_id/posts/:id
    def update
      @post.authors = Author.from_s(params[:authors]) if params[:authors]
      @post.user = current_user

      if @post.update(post_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_issue_post_url(@issue, @post)
      else
        flash.now[:alert] = t('messages.error.save')
        @title = @issue.title
        @subtitle = @post.title
        render :form
      end
    end

    # Delete /admin/issues/:issue_id/posts/:id
    def destroy
      if @post.destroy
        flash[:notice] = t('messages.post.success.delete', title: @post.title)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_issue_posts_url
    end

    private

    def post_params
      params.require(:post)
            .permit(:title, :subtitle, :body, :published,
                    attachment_set_attributes: [:id],
                    issue_attributes: [:id])
    end

    def find_issue
      @issue = Issue.find(params[:issue_id])
    end

    def find_post
      @post = Post.find(params[:id])
    end

    def find_authors
      @authors = Author.pluck(:name)
    end
  end
end
