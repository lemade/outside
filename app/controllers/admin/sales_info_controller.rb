# frozen_string_literal: true

module Admin
  class SalesInfoController < Admin::ApplicationController
    # GET /admin/sales_info/edit
    def edit
      @sales_info = SalesInfo.new
      @title = @sales_info.title
      render :form
    end

    # PATCH /admin/sales_info/:id
    def update
      @sales_info = SalesInfo.new

      if @sales_info.update_attributes(params[:sales_info]) # rubocop:disable Rails/ActiveRecordAliases
        flash[:notice] = t('messages.success.save')
        redirect_to admin_stores_url
      else
        flash.now[:alert] = t('messages.error.save')
        @title = @sales_info.title
        render :form
      end
    end
  end
end
