# frozen_string_literal: true

module Admin
  class StoresController < Admin::ApplicationController
    before_action :find_countries, only: %i[new create edit update]
    before_action :find_store,     only: %i[edit update destroy]

    # DET /admin/stores
    def index
      @sales_info = SalesInfo.new
      @stores = Store.search(params).page(params[:page]).limit(15)
      @countries = Country.with_stores.uniq
      @title = t('labels.store.index')
    end

    # GET /admin/stores/new
    def new
      @store = Store.new
      @title = t('labels.store.menu')
      @subtitle = t('labels.store.new')
      render :form
    end

    # POST /admin/stores
    def create
      @store = Store.new(store_params)

      if @store.save
        flash[:notice] = t('messages.store.success.save', name: @store.name)
        redirect_to admin_stores_url
      else
        flash.now[:alert] = t('messages.store.error.save')
        @title = t('labels.store.menu')
        @subtitle = t('labels.store.new')
        render :form
      end
    end

    # GET /admin/stores/:id/edit
    def edit
      @country = @store.country
      @title = t('labels.store.menu')
      @subtitle = @store.name
      render :form
    end

    # PUT /admin/stores/:id
    def update
      if @store.update(store_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_stores_url
      else
        flash.now[:alert] = t('messages.error.save')
        @title = t('labels.store.menu')
        @subtitle = @store.name
        render :form
      end
    end

    # DELETE /admin/stores/:id
    def destroy
      if @store.destroy
        flash[:notice] = t('messages.store.success.delete', name: @store.name)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_stores_url
    end

    private

    def store_params
      params.require(:store)
            .permit(:name, :direction, :url,
                    city_attributes: %i[id city country_id])
    end

    def find_countries
      @countries = Country.all
    end

    def find_store
      @store = Store.find(params[:id])
    end
  end
end
