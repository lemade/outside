# frozen_string_literal: true

module Admin
  class SubscribersController < Admin::ApplicationController
    # GET /admin/subscribers
    def index
      @newsletter_info = NewsletterInfo.new
      @subscribers = Subscriber.search(params).page(params[:page]).limit(15).ordered
      @title = t('labels.subscriber.index')
    end

    # DELETE /admin/subscribers/:id
    def destroy
      @subscriber = Subscriber.find(params[:id])

      if @subscriber.destroy
        NewsletterMailer.delay.admin_unsubscribe_mail(@subscriber)
        flash[:notice] = t('messages.subscriber.success.delete')
      else
        flash[:alert] = t('messages.delete.error')
      end

      redirect_to admin_subscribers_url
    end
  end
end
