# frozen_string_literal: true

module Admin
  class UsersController < Admin::ApplicationController
    skip_before_action :authorize, only: %i[profile update_profile]
    before_action :find_user,      only: %i[edit update destroy]

    # GET /admin/profile
    def profile
      if current_user.admin?
        redirect_to edit_admin_user_url(current_user)
      else
        @user = current_user
        @title = t('labels.user.profile')
      end
    end

    # PUT /admin/profile
    def update_profile
      @user = current_user
      @user.check_old_password_before_save

      if @user.update(user_params)
        flash.now[:notice] = t('messages.success.save')
      else
        flash.now[:alert] = t('messages.error.save')
      end

      @title = t('labels.user.profile')
      render :profile
    end

    # GET /admin/users
    def index
      @users = User.search(params).page(params[:page]).limit(15).ordered
      @title = t('labels.user.index')
    end

    # GET /admin/users/new
    def new
      @user = User.new
      @title = t('labels.user.new')
      render :form
    end

    # POST /admin/users
    def create
      @user = User.new(user_params)

      if @user.save
        flash[:notice] = t('messages.user.success.save', name: @user.username)
        redirect_to admin_users_url
      else
        flash.now[:alert] = t('messages.user.error.save')
        render :form
      end
    end

    # GET /admin/users/:id/edit
    def edit
      @title = t('labels.user.edit')
      @subtitle = @user.username
      render :form
    end

    # PUT /admin/users/:id
    def update
      if @user.update(user_params)
        flash[:notice] = t('messages.success.save')
        redirect_to admin_users_url
      else
        flash.now[:alert] = t('messages.error.save')
        render :form
      end
    end

    # DELETE /admin/users/:id
    def destroy
      if @user == current_user
        flash[:alert] = t('messages.user.error.delete_self')
        redirect_to(admin_users_url) && return
      end

      if @user.destroy
        flash[:notice] = t('messages.user.success.delete', name: @user.username)
      else
        flash[:alert] = t('messages.error.delete')
      end

      redirect_to admin_users_url
    end

    private

    def user_params
      if current_user&.admin?
        params.require(:user)
              .permit(:username, :password, :password_confirmation, :admin)
      else
        params.require(:user)
              .permit(:password, :password_confirmation, :password_old)
      end
    end

    def find_user
      @user = User.find(params[:id])
    end
  end
end
