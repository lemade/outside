# frozen_string_literal: true

module Ajax
  class ApplicationController < Admin::ApplicationController
    before_action :check_xhr

    skip_before_action :authorize

    layout false

    respond_to :html

    private

    # authenticated user
    def authenticate
      error(t('messages.authentication.error'), 401) unless current_user || Rails.env.development?
    end

    # check for XmlHttpRequest
    def check_xhr
      redirect_to(admin_url) unless request.xhr? || Rails.env.development?
    end

    # renders an error message and sets the response status code
    def error(message, status = 422)
      render plain: message, status: status
    end
  end
end
