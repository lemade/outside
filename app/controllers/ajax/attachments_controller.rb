# frozen_string_literal: true

module Ajax
  class AttachmentsController < Ajax::ApplicationController
    before_action :find_attachment_set
    respond_to :json, only: [:update]

    # GET /ajax/attachment_sets/:attachment_set_id/attachments
    def index; end

    # POST /ajax/attachment_sets/:attachment_set_id/attachments
    def create
      attachment = @attachment_set.attachments.build(attachment_params)

      if attachment.save!
        render plain: url_for(attachment.picture.variant(resize_to_limit: [800, 600]))
      else
        error(t('messages.attachment.error.save'), 500)
      end
    end

    # POST /ajax/attachment_sets/:attachment_set_id/attachments/sort
    def sort
      @attachment_set.sort_attachments(Array.wrap(params[:attach]).map(&:to_i))
      head :ok
    end

    # PUT /ajax/attachment_sets/:attachment_set_id/attachments/:id
    def update
      @attachment = @attachment_set.attachments.find(params[:id])
      @attachment.update(attachment_params)

      respond_with @attachment
    end

    # DELETE /ajax/attachment_sets/:attachment_set_id/attachments/:id
    def destroy
      attachment = @attachment_set.attachments.find(params[:id])
      attachment&.picture&.purge

      if attachment.destroy
        head :ok
      else
        error(t('messages.error.delete'), 500)
      end
    end

    private

    def attachment_params
      params.require(:attachment).permit(:caption, :picture, attachment_set_attributes: [:id])
    end

    def find_attachment_set
      @attachment_set = AttachmentSet.find(params[:attachment_set_id])
    end
  end
end
