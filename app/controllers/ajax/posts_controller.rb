# frozen_string_literal: true

module Ajax
  class PostsController < Ajax::ApplicationController
    # ajax/posts/sort
    def sort
      Post.sort(Array.wrap(params[:art]).map(&:to_i))
      head :ok
    end
  end
end
