# frozen_string_literal: true

module Ajax
  class UploadsController < Ajax::ApplicationController
    # GET /ajax/uploads
    def index
      @uploads = Upload.all
    end

    # POST /ajax/uploads
    def create
      upload = Upload.new(upload_params)

      if upload.save!
        render plain: url_for(upload.dossier)
      else
        error(t('messages.upload.error.save'), 500)
      end
    end

    # DELETE /ajax/uploads/:id
    def destroy
      upload = Upload.find(params[:id])
      upload&.dossier&.purge

      if upload.destroy
        head :ok
      else
        error(t('messages.upload.error.delete'), 500)
      end
    end

    private

    def upload_params
      params.require(:upload).permit(:dossier)
    end
  end
end
