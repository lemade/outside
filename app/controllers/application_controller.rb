# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  protect_from_forgery

  helper_method :body_class
  helper_method :current_user
  helper_method :current_user?

  def body_class
    unless @body_class
      @body_class = params[:controller].split(%r{/})
      @body_class << params[:action]
    end
    @body_class
  end

  protected

  def not_found
    render plain: '404 not found', status: :not_found
  end

  def current_user
    login_hash = session[:login_hash]
    @current_user ||= User.find_by(login_hash: login_hash) if login_hash
  end

  def current_user?(user)
    user == current_user
  end
end
