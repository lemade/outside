# frozen_string_literal: true

class DsgvoController < PublicController
  # GET /datenschutz
  def show
    @dsgvo = Dsgvo.new
    @title = @dsgvo.title
  end
end
