# frozen_string_literal: true

class ImpressumController < PublicController
  # GET /impressum
  def show
    @impressum = Impressum.new
    @title = @impressum.title
  end
end
