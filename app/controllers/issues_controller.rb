# frozen_string_literal: true

class IssuesController < PublicController
  # GET /issues
  def index
    @issues = Issue.all
  end
end
