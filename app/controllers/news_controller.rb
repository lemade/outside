# frozen_string_literal: true

class NewsController < PublicController
  # GET /news
  def index
    @all_news = News.published.page(params[:page]).limit(10).ordered
    @news = News.published.ordered.first
    @title = t('labels.news.public')
    render :show
  end

  # GET /news/:id
  def show
    @all_news = News.published.page(params[:page]).limit(10).ordered
    @news = News.find(params[:id])
    @title = t('labels.news.public')
  end
end
