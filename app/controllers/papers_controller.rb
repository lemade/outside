# frozen_string_literal: true

class PapersController < PublicController
  # GET /papers
  def index
    @papers = Paper.published.page(params[:page]).limit(10).ordered
    @paper = Paper.published.ordered.first
    @title = t('labels.paper.public')
    render :show
  end

  # GET /papers/:id
  def show
    @papers = Paper.published.page(params[:page]).limit(10).ordered
    @paper = Paper.find(params[:id])
    @authors = Author.pluck(:name)
    @title = t('labels.paper.public')
  end
end
