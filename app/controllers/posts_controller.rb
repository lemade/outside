# frozen_string_literal: true

class PostsController < PublicController
  before_action :find_issue, only: %i[index show]
  before_action :find_posts, only: %i[index show]
  before_action :find_post,  only: %i[index show]

  # GET /issues/:issue_id/posts
  def index
    @title = @issue.title
    render :show
  end

  # GET /issues/:issue_id/posts/:id
  def show
    @title = @issue.title
  end

  private

  def find_issue
    @issue = Issue.find(params[:issue_id])
  end

  def find_posts
    @posts = @issue.posts.published
  end

  def find_post
    @post = params.key?(:id) ? @posts.find(params[:id]) : @posts.first
  end
end
