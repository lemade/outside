# frozen_string_literal: true

class PublicController < ApplicationController
  rescue_from ActionController::RoutingError, with: :not_found

  layout 'public'

  def index
    @news = News.published.ordered.staged
  end

  private

  def not_found(err)
    raise err unless Rails.env.production?

    flash[:alert] = t('messages.error.page_not_found')
    redirect_to root_url
  end
end
