# frozen_string_literal: true

class SearchController < PublicController
  # GET /search
  def show
    @title = t('labels.search.public')
    @papers = []
    @issues = []

    return if params[:commit].blank?

    if params[:category].blank?
      @papers = Paper.published.search(params)
      @issues = Issue.reverse_published.search_posts(params)
    elsif params[:category] == 'posts'
      @issues = Issue.reverse_published.search_posts(params)
    elsif params[:category] == 'papers'
      @papers = Paper.published.search(params)
    end

    flash.now[:notice] = t('messages.error.find') if @papers.blank? && @issues.blank?
  end
end
