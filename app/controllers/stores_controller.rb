# frozen_string_literal: true

class StoresController < PublicController
  # GET /stores(/:country)
  def index
    @countries = Country.with_stores.ordered
    @sales_info_body = SalesInfo.new.body

    @country = if params[:country].present?
                 @countries.find { |c| c.country == params[:country] }
               else
                 @countries.first
               end

    # Raise 404 when country code in url not found
    if @countries.length.positive? && @country.nil?
      raise ActionController::RoutingError, t('messages.country.error.not_found')
    end

    @title = t('labels.store.public')
  end
end
