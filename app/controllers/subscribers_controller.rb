# frozen_string_literal: true

class SubscribersController < PublicController
  before_action :find_subscriber, only: %i[confirm unsubscribe]
  before_action :verify_url,      only: %i[confirm unsubscribe]

  # GET /subscribers
  def index
    @title = t('labels.subscriber.public.new')
  end

  # GET /subscribers/new
  def new
    @newsletter_info_text = NewsletterInfo.new.body
    @subscriber = Subscriber.new
    @title = t('labels.subscriber.public.new')
    render :form
  end

  # POST /subcribers/
  def create
    @subscriber = Subscriber.new(subscriber_params)
    @title = t('labels.subscriber.public.new')

    if verify_rucaptcha?(@subscriber) && @subscriber.save
      flash[:notice] = t('messages.subscriber.success.save')
      redirect_to subscribers_path
    else
      @subscriber.errors.add(:captcha, t('rucaptcha.invalid'))
      flash.now[:alert] = t('messages.subscriber.error')
      render :form
    end
  end

  # GET /subscribers/:id/confirm
  def confirm
    if @subscriber.update(
      confirmed: true,
      nl_consent_text: I18n.t('messages.subscriber.nl-consent'),
      nl_consent_date: Time.current
    )
      flash[:notice] = t('messages.subscriber.success.confirm')
    else
      flash[:alert] = t('messages.subscriber.error')
    end

    redirect_to root_url
  end

  # GET /subscribers/:id/unsubscribe
  def unsubscribe
    if @subscriber.destroy
      flash[:notice] = t('messages.subscriber.success.unsubscribe')
    else
      flash[:alert] = t('messages.subscriber.error')
    end

    redirect_to root_url
  end

  private

  def subscriber_params
    params.require(:subscriber).permit(:email)
  end

  def find_subscriber
    @subscriber = Subscriber.find(params[:id])
  end

  def verify_url
    return if @subscriber.verify_url(request.original_url, params[:signature].to_s)

    flash[:alert] = t('messages.subscriber.error')
    redirect_to root_url
  end
end
