# frozen_string_literal: true

require 'kramdown'

module ApplicationHelper
  # Renders markdown formatted text.
  def markdown(txt)
    return '' if txt.nil?

    tag.div(class: 'markdown') do
      Kramdown::Document.new(txt, auto_links: true, escape_html: true).to_html.html_safe # rubocop:disable Rails/OutputSafety
    end
  end

  # Select tag for countrys in stores index filter
  def country_select_tag(countries, name, selected = nil, options = {})
    select_tag(
      name,
      options_from_collection_for_select(countries, :id, :human, selected),
      options
    )
  end

  def published_status(published)
    if published
      content_tag(:span, I18n.t('true', scope: %i[labels published]), class: 'fas fa-eye')
    else
      content_tag(:span, I18n.t('false', scope: %i[labels published]), class: 'fas fa-eye-slash')
    end
  end

  def on_stage_status(on_stage)
    if on_stage
      content_tag(:span, I18n.t('true', scope: %i[labels on-stage long]), class: 'fas fa-bell')
    else
      content_tag(:span, I18n.t('false', scope: %i[labels on-stage long]), class: 'fas fa-bell-slash')
    end
  end
end
