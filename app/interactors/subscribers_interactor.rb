# frozen_string_literal: true

class SubscribersInteractor
  include Delayed::RecurringJob

  run_every 1.day
  run_at '1:00am'
  timezone Outside::Application.config.time_zone
  queue 'slow-jobs'

  def perform
    Subscriber.where(confirmed: false).where('created_at < ?', 1.week.ago).delete_all
  end
end
