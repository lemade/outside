import '@fortawesome/fontawesome-free/css/all.css'

require('jquery-ui')

require.context('../images', true)
require('../stylesheets/admin.scss')
require('../stylesheets/admin/sections/aether.scss')
require('../src/completeList')
require('../src/galleries')
require('../src/listItems')
require('../src/mdToolbar')
require('../src/renderShow')
require('../src/sortTable')
require('../src/toggle_datepicker')
require('../src/tooltip')
require('../vendor/best_in_place')

jQuery(window).on('load', function () {
  // Let's go.
  $(document).ready(function() {
    $('input[data-auto-submit="true"]').autoSubmit();
    $('.best_in_place').best_in_place();
    if($('#list-authors').length) {
      $('#list-authors').completeList($('#list-authors').data('authors') || 'something');
    }
    if($('#list-countries').length) {
      $('#list-countries').listItems($('#list-countries').data('countries'));
    }
    $('ul.md-toolbar').mdToolbar('textarea.body');
    $('div.show').postPreview();
    $('div.markdown img').renderImage();
    $('#attachments').sortTable($('#attachments').data('url'), {items: 'div.attachment'});
    $('#post-index-table').sortTable($('#post-index-table').data('url'));
    $('a#info').tooltip('#tooltip');
  });
});
