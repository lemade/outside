// completeList.js
//
// jQuery outsidePlugin for autocomplete a ', ' seperated list.
// The first param must be an array with valid values.
(function($) {
  $.fn.completeList = function(list) {
    return this.each(function() {
      $(this).autocomplete({
        minLength: 0,
        appendTo: $(this).parent(),
        source: function(req, res) {
          res($.ui.autocomplete.filter(list, req.term.split(/,\s*/).pop()));
        },
        focus: function() {
          return false;
        },
        select: function(e, ui) {
          var terms = this.value.split(/,\s*/);
          terms.pop();
          terms.push(ui.item.value);
          terms.push('');
          this.value = terms.join(', ');
          return false;
        }
      });
    });
  };
})(jQuery);
