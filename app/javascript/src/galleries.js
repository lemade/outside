// galleries.js
//
// jQuery outsidePlugin to handle automatic submit images to galleries
(function($) {
  $.fn.autoSubmit = function() {
    return this.each(function() {
      $(this).change(function() {
        $(this).closest('form').submit();
      });
    });
  };
})(jQuery);
