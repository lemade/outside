(function($) {
  $.fn.ImgView = function() {
    return this.each(function() {
      var el = $(this).wrap('<a>');
      var src = el.attr('src').split('/');
      src[src.length-2] = 'big';
      el.parent().attr('href', src.join('/'));
    });
  };
})(jQuery);
