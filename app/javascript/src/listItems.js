// listItems.js
//
// JQuery outsidePlugin to handle list items
(function($) {
  $.fn.listItems = function(list) {
    var map = {};
    var src = [];

    for (var i = 0, len = list.length; i < len; i++) {
      map[list[i].human] = list[i].id;
      src.push(list[i].human);
    }
    var hidden = $(this).next('input[type="hidden"]');

    return $(this).each(function() {
      $(this).autocomplete({
        minLength: 1,
        appendTo: $(this).parent(),
        source: src,
        select: function(e, ui) {
          hidden.val(map[ui.item.value]);
        }
      });
    });
  };
})(jQuery);
