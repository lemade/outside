// mdtoolbar.js
//
// jQuery outsidePlugin for the markdown toolbar
(function($) {
  /**
  * Markdown toolbar
  *
  * Handles a markdown toolbar. Needs a selector specifying the textarea and
  * 2 uris for the file chooser.
  */
  $(document).ajaxSend(function(e, xhr, options) {
    var sid = $("meta[name='csrf-token']").attr("content");
    xhr.setRequestHeader("X-CSRF-Token", sid);
  });

  $.fn.mdToolbar = function(selector) {
    var Area = function(e) {
      this.start = e.selectionStart;
      this.end = e.selectionEnd;
      this.txt = e.value;
      this.el = e;
    };

    Area.prototype.setTxt = function(txt) {
      this.el.value = txt;
      this.el.selectionStart = this.el.selectionEnd = this.end + txt.length - this.txt.length;
      this.txt = txt;
      this.start = this.end = this.el.selectionEnd;
    };

    Area.prototype.insert = function(c) {
      this.setTxt(this.txt.substr(0, this.start) + c + this.txt.substr(this.start));
    };

    Area.prototype.wrap = function(c) {
      if (this.start === this.end) {
        return this.insert(c);
      }

      this.setTxt(this.txt.substr(0, this.start) + c +
                   this.txt.substr(this.start, this.end - this.start) + c +
                  this.txt.substr(this.end));
    };

    Area.prototype.insertNextLine = function(c) {
      var i = this.txt.indexOf('\n', this.end);
      this.start = this.end = (i === -1) ? this.txt.length : i;
      this.insert('\n' + c + ' ');
    };

    Area.prototype.insertBeforeFirst = function(c, multiple) {
      if (this.start > 0) {
        this.start = this.txt.lastIndexOf('\n', this.start - 1)
      }

      this.start !== 0 && this.start++;

      if (this.txt.charAt(this.start) != c) {
        c += ' ';
      } else if ( ! multiple) {
        return this.insertNextLine(c);
      }

      this.insert(c);
    };

    Area.prototype.insertAtEnd = function(c) {
      this.start = this.end = this.txt.length;
      this.insert('\n' + c);
    };

    return this.each(function() {
      var el = $(this).next(selector).get(0);
      var noteCounter = 0;

      $(this).children('li').click(function() {
        var area = new Area(el),
            btn = $(this);

        if (btn.hasClass('i')) {
          area.wrap('_');
        } else if (btn.hasClass('b')) {
          area.wrap('**');
        } else if(btn.hasClass('h')) {
          area.insertBeforeFirst('#', true);
        } else if(btn.hasClass('q')) {
          area.insertBeforeFirst('>', true);
        } else if(btn.hasClass('fn')) {
          noteCounter++;
          area.insert('[^'+ noteCounter +']');
          area.insertAtEnd('[^'+ noteCounter +']: ');
        } else if(btn.hasClass('li')) {
          area.insertBeforeFirst('-', false);
        } else if(btn.hasClass('a')) {
          area.insert('[text](uri)');
        } else if(btn.hasClass('img')) {
          fileChooser(btn.data('url'), function(img_url) {
            area.insert('<span class="single-img">![caption](' + img_url + ')');
          });
        } else if(btn.hasClass('file')) {
          fileChooser(btn.data('url'), function(file_url) {
            area.insert('[name](' + file_url + ')');
          });
        } else if (btn.hasClass('help')) {
          return true;
        }
        return false;
      });
    });
  };

  /**
   * fileChooser
   *
   * renders a file chooser to upload pictures in mdtoolbar
   */
  var fileChooser = function(url, callback) {
    $.get(url, function(html) {
      var layer = mkLayer(),
          overlay = $(html),
          form = overlay.find('form');

      overlay.find('li[data-url]').click(function() {
        callback($(this).data('url'));
        overlay.remove();
        layer.remove();
      });

      overlay.find('.close').click(function() {
        overlay.remove();
        layer.remove();
      });

      overlay.find('a.del').click(function() {
        var a = $(this);

        $.ajax({
          url: a.attr('href'),
          type: 'delete',
          success: function() {
            a.parent().remove();
          },
          error: function(xhr, _) {
            overlay.find('span.msg').text(xhr.responseText);
          }
        });

        return false;
      });

      form.submit(function() {
        var data = new FormData(this);

        $.ajax({
          url: url,
          type: 'post',
          data: data,
          success: function(data) {
            callback(data);
            overlay.remove();
            layer.remove();
          },
          error: function(xhr, _) {
            overlay.find('span.msg').text(xhr.responseText);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
      });

      form.find('input[type=file]').change(function() {
        form.submit();
      });

      $('body').append(layer).append(overlay);
    });
  };

  /**
   * mkLayer
   *
   * renders the layer for the fileChooser
   */
  var mkLayer = function() {
    return $('<div>', {class: 'layer'});
  };
})(jQuery);
