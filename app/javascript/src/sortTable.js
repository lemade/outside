// sortTable.js
//
// jQuery outsidePlugin for sorting table rows
(function($) {
  $.fn.sortTable = function(url, options) {
    csrfParam = $('meta[name="csrf-param"]').attr('content');
    csrfToken = $('meta[name="csrf-token"]').attr('content');

    options = $.extend({
        items: 'tr',
        cursor: 'move',
        update: function() {
          data = $(this).sortable('serialize');
          data += '&' + csrfParam + '=' + csrfToken;
          $.ajax({
            type: 'post',
            url: url,
            data: data,
            dataType: 'script'
          });
        }
      }, options);

    return this.each(function() {
      $(this).sortable(options).disableSelection();
    });
  };
})(jQuery);
