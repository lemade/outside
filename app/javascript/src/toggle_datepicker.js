toggle_datepicker = function() {
    var datepicker = $('.on-stage-period')
    if ($('#news_on_stage').is(':checked')) {
        $(datepicker).show();
    } else {
        $(datepicker).hide();
    }
};

$(document).ready(toggle_datepicker);

$(document).on('click', toggle_datepicker);