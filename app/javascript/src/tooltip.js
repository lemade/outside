// tooltip.js
//
// Defines the jQuery outsideTooltip plugin.
(function($) {
  $.fn.tooltip = function(tip) {
    return this.each(function() {
      $(this).click(function(event) {
        event.preventDefault();
        $(tip).stop().fadeToggle();
      });
    });
  };
})(jQuery);
