# frozen_string_literal: true

class NewsletterMailer < ApplicationMailer
  def welcome_email(subscriber)
    @subscriber = subscriber
    mail(to: subscriber.email,
         subject: "Willkommen zur 'Outside The Box' Newsletter Liste")
  end

  def admin_unsubscribe_mail(subscriber)
    @subscriber = subscriber
    @url = 'http://outside-mag.de/subscribers/new'
    mail(to: subscriber.email,
         subject: "Du wurdest aus dem 'Outside the Box' Newsletter ausgetragen")
  end

  def send_newsletter(subscriber, newsletter)
    @subscriber = subscriber
    @newsletter = newsletter
    mail(to: subscriber.email,
         subject: @newsletter.title)
  end
end
