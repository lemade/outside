# frozen_string_literal: true

class About
  include KVModel

  kv :Setting, :title, :body, :author
end
