# frozen_string_literal: true

class Attachment < ApplicationRecord
  belongs_to :attachable, polymorphic: true
  has_one_attached :picture, dependent: :purge

  validates :picture, content_type: %i[png jpg jpeg gif], size: {less_than: 5.megabytes}

  default_scope { order(:position) }
  before_create :add_position

  private

  def add_position
    self.position = Attachment.count
  end
end
