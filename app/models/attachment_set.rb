# frozen_string_literal: true

class AttachmentSet < ApplicationRecord
  has_many :attachments, as: :attachable, dependent: :destroy

  accepts_nested_attributes_for :attachments, reject_if: ->(t) { t['picture'].nil? }

  def sort_attachments(ids)
    attachments.where(id: ids).find_each do |attachment|
      attachment.update(position: ids.index(attachment.id))
    end
  end
end
