# frozen_string_literal: true

require 'set'

class Author < ApplicationRecord
  has_and_belongs_to_many :posts
  has_and_belongs_to_many :papers
  has_many :issues, through: :posts

  validates :name, presence: true, uniqueness: {case_sensitive: true}

  def self.ordered
    order('name')
  end

  def self.search(params)
    result = self

    result = result.where('name LIKE ?', "%#{params[:name]}%") if params[:name].present?

    result = result.where(id: published_ids) if params[:has_published].present? && params[:has_published] == '1'

    result = result.where.not(id: published_ids) if params[:has_published].present? && params[:has_published] == '0'

    result
  end

  def self.from_s(str, delimiter = /,\s*/)
    authors = Set.new

    str.split(delimiter).each do |author|
      author.strip!
      authors << author unless author.empty?
    end

    authors.empty? ? [] : multi_find_or_initialize_by_author(authors.to_a)
  end

  def self.multi_find_or_initialize_by_author(authors)
    ret = where(name: authors).each { |author| authors.delete author.name } # rubocop:disable Rails/FindEach
    ret + authors.collect { |author| new(name: author) }
  end

  # returns authors id who have any post or paper
  def self.published_ids
    ids = Post.joins(:authors).pluck('authors.id')
    ids + Paper.joins(:authors).pluck('authors.id')
  end

  # Returns true if author has at least one post or paper
  def published_yet?
    %i[posts papers].any? { |a| send(a).count.positive? }
  end

  def human_published
    I18n.t(published_yet?.to_s, scope: %i[labels has_published])
  end

  def to_s
    name
  end
end
