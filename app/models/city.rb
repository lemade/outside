# frozen_string_literal: true

class City < ApplicationRecord
  has_many :stores, dependent: :destroy
  belongs_to :country

  validates :city, presence: true
  validates :country_id, presence: true

  default_scope { order(:city) }

  def self.with_stores
    joins(:stores).group('cities.id')
  end

  def self.find_or_init_city(params)
    City.find_or_initialize_by(
      city: params[:city],
      country_id: params[:country_id]
    )
  end
end
