# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :cities, dependent: :destroy
  has_many :stores, through: :cities

  validates :country, presence: true, uniqueness: {case_sensitive: true}

  def self.with_stores
    joins(:stores)
  end

  # order countries by count of stores
  def self.ordered
    select('countries.*, COUNT(stores.id) AS x')
      .joins(:stores)
      .group('countries.id')
      .order('x DESC')
  end

  def human
    I18n.t(country, scope: :countries)
  end

  def as_json(options = {})
    super(options.merge(methods: :human))
  end
end
