# frozen_string_literal: true

class Dsgvo
  include KVModel

  kv :Setting, :title, :body
end
