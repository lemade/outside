# frozen_string_literal: true

class Gallery < ApplicationRecord
  belongs_to :post
  belongs_to :attachment_set, dependent: :destroy
  has_many :attachments, through: :attachment_set

  validates :post_id, presence: true

  after_initialize :add_attachment_set

  private

  def add_attachment_set
    create_attachment_set unless attachment_set
  end
end
