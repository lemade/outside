# frozen_string_literal: true

class Impressum
  include KVModel

  kv :Setting, :title, :body
end
