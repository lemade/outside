# frozen_string_literal: true

class Issue < ApplicationRecord
  has_many :posts, -> { order :position }, dependent: :destroy, inverse_of: :issue
  has_one_attached :picture

  validates :title, presence: true, uniqueness: {case_sensitive: true}
  validates :picture, content_type: %i[png jpg jpeg gif], size: {less_than: 5.megabytes}

  # Get all published
  def self.published
    where(published: true)
  end

  # Get all published in reverse order
  def self.reverse_published
    published.reverse_order
  end

  # Find the previous entry
  def prev
    Issue.published.where('created_at < ?', created_at).last
  end

  # Find the previous entry if any
  def prev?
    !prev.nil?
  end

  # Find the next entry
  def next
    Issue.published.find_by('created_at > ?', created_at)
  end

  # Find the next entry if any
  def next?
    !self.next.nil?
  end

  # Search parameters for the public frontend index filter
  def self.search_posts(params)
    result = published.includes(:posts).joins(:posts).where('posts.published = ?', true)

    result = result.where(id: params[:issue_id]) if params[:issue_id].present?

    result = result.where('posts.title LIKE ?', "%#{params[:title]}%") if params[:title].present?

    if params[:author].present?
      result = result.joins('INNER JOIN authors_posts ap ON ap.post_id = posts.id')
      result = result.joins('INNER JOIN authors ON author_id = authors.id')
      result = result.where('authors.name LIKE ?', "%#{params[:author]}%").uniq
    end

    result = result.where('MATCH(posts.body) AGAINST (? IN BOOLEAN MODE)', params[:body]) if params[:body].present?

    result
  end
end
