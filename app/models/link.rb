# frozen_string_literal: true

class Link < ApplicationRecord
  validates :name, presence: true
  validates :url, presence: true, format: URI::DEFAULT_PARSER.make_regexp(%w[http https]), allow_blank: true

  default_scope { order(:name) }

  def self.search(params)
    result = self

    result = result.where('name LIKE ?', "%#{params[:name]}%") if params[:name].present?

    result = result.where('url LIKE ?', "%#{params[:url]}%") if params[:url].present?

    result
  end
end
