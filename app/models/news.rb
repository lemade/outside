# frozen_string_literal: true

class News < ApplicationRecord
  belongs_to :user, optional: false
  belongs_to :attachment_set, optional: false, dependent: :destroy
  has_many :attachments, through: :attachment_set

  validates :title, presence: true
  validates :on_stage_from, :on_stage_until, presence: true, if: :on_stage
  validate :on_stage_from_before_until

  def self.ordered
    order(updated_at: :desc)
  end

  # Find first published entry
  def first
    News.published.ordered.first
  end

  # Find the previous entry
  def prev
    News.published.where('updated_at > ?', updated_at).ordered.last
  end

  # Find the previous entry if any
  def prev?
    !prev.nil?
  end

  # Find the next entry
  def next
    News.published.where('updated_at < ?', updated_at).ordered.first
  end

  # Find the next entry if any
  def next?
    !self.next.nil?
  end

  # Search parameters for the public and admin frontend index filter
  def self.search(params, user = nil)
    result = user.present? ? self : published

    result = result.where('title LIKE ?', "%#{params[:title]}%") if params[:title].present?

    result = result.where(published: params[:published]) if params[:published].present?

    if params[:author].present?
      result = result.joins(:authors).where('authors.name LIKE ?', "%#{params[:author]}%").uniq
    end

    result = result.where('MATCH(body) AGAINST (? IN BOOLEAN MODE)', params[:body]) if params[:body].present?

    result
  end

  # Get all published
  def self.published
    where(published: true)
  end

  def human_published
    I18n.t(published?.to_s, scope: %i[labels published])
  end

  # Get all on stage
  def self.staged
    where(published: true).where(on_stage: true).where('on_stage_until > ?', Date.today)
  end

  def on_stage?
    self.published && self.on_stage && self.on_stage_until > Date.today
  end

  def human_on_stage
    I18n.t(on_stage?.to_s, scope: %i[labels on-stage short])
  end

  def attachment_set
    super || create_attachment_set
  end

  private

  def on_stage_from_before_until
    if on_stage && on_stage_from.try(:>, on_stage_until)
      errors.add(:on_stage_until, I18n.t('activerecord.errors.messages.date_invalid'))
    end
  end
end
