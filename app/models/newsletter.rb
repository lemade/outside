# frozen_string_literal: true

class Newsletter < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :title, presence: true

  default_scope { order(updated_at: :desc) }

  def self.published
    where(published: true)
  end

  def self.unpublished
    where(published: false)
  end

  def send_to(subscribers)
    subscribers.each do |s|
      NewsletterMailer.delay.send_newsletter(s, self)
    end

    update(published: true)
  end
end
