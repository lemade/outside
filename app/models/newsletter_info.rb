# frozen_string_literal: true

class NewsletterInfo
  include KVModel

  kv :Setting, :title, :body
end
