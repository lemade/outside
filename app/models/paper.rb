# frozen_string_literal: true

class Paper < ApplicationRecord
  belongs_to :user, optional: false
  belongs_to :attachment_set, optional: false, dependent: :destroy
  has_many :attachments, through: :attachment_set
  has_and_belongs_to_many :authors

  validates :title, presence: true

  def self.ordered
    order(updated_at: :desc)
  end

  # Find first published entry
  def first
    Paper.published.ordered.first
  end

  # Find the previous entry
  def prev
    Paper.published.where('updated_at > ?', updated_at).ordered.last
  end

  # Find the previous entry if any
  def prev?
    !prev.nil?
  end

  # Find the next entry
  def next
    Paper.published.where('updated_at < ?', updated_at).ordered.first
  end

  # Find the next entry if any
  def next?
    !self.next.nil?
  end

  # Search parameters for the public and admin frontend index filter
  def self.search(params, user = nil)
    result = user.present? ? self : published

    result = result.where('title LIKE ?', "%#{params[:title]}%") if params[:title].present?

    result = result.where(published: params[:published]) if params[:published].present?

    if params[:author].present?
      result = result.joins(:authors).where('authors.name LIKE ?', "%#{params[:author]}%").uniq
    end

    result = result.where('MATCH(body) AGAINST (? IN BOOLEAN MODE)', params[:body]) if params[:body].present?

    result
  end

  # Get all published
  def self.published
    where(published: true)
  end

  def human_published
    I18n.t(published?.to_s, scope: %i[labels published])
  end

  def attachment_set
    super || create_attachment_set
  end
end
