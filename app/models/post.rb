# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :issue, inverse_of: :posts, optional: false
  belongs_to :user, optional: false
  belongs_to :attachment_set, optional: false, dependent: :destroy
  has_many :attachments, through: :attachment_set
  has_and_belongs_to_many :authors
  has_one :gallery, dependent: :destroy

  validates :title, presence: true

  before_create :add_position

  def self.ordered
    order(position: :asc)
  end

  # Find first entry of an issue
  def first
    Post.published.where('issue_id = :issue_id', issue_id: issue_id).ordered.first
  end

  # Find the previous entry
  def prev
    Post.published.where('issue_id = :issue_id AND position < :pos',
                         issue_id: issue_id, pos: position).ordered.last
  end

  # Find the previous entry if any
  def prev?
    !prev.nil?
  end

  # Find the next entry
  def next
    Post.published.where('issue_id = :issue_id AND position > :pos',
                         issue_id: issue_id, pos: position).ordered.first
  end

  # Find the next entry if any
  def next?
    !self.next.nil?
  end

  # Search parameters for the public frontend index filter
  def self.search(params)
    result = scoped

    result = result.where('title LIKE ?', "%#{params[:title]}%") if params[:title].present?

    if params[:author].present?
      result = result.joins(:authors).where('authors.name LIKE ?', "%#{params[:author]}%").uniq
    end

    result
  end

  # Create attachment set and get id
  def attachment_set
    super || create_attachment_set
  end

  def self.sort(ids)
    connection.transaction do
      where(id: ids).find_each do |post|
        post.update(position: ids.index(post.id))
      end
    end
  end

  # Get all published
  def self.published
    where(published: true)
  end

  # Get Gallery if any
  def gallery?
    !gallery.nil?
  end

  def human_published
    I18n.t(published?.to_s, scope: %i[labels published])
  end

  private

  def add_position
    self.position = Post.count
  end
end
