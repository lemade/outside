# frozen_string_literal: true

class SalesInfo
  include KVModel

  kv :Setting, :title, :body
end
