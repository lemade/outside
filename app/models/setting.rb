# frozen_string_literal: true

class Setting < ApplicationRecord
  def self.set(settings = {})
    set!(settings)
  rescue StandardError
    false
  end

  def self.set!(settings = {})
    connection.transaction do
      multi_find_or_initialize_by_key(settings.keys).each do |setting|
        setting.update(value: settings[setting.key])
      end
    end
  end

  def self.get(*keys)
    hsh = {}
    multi_find_or_initialize_by_key(keys).each { |s| hsh[s.key] = s.value }
    hsh
  end

  def self.get!(*keys)
    settings = where(key: keys)

    if settings.length == keys.length
      settings.map { |s| {s.key => s.value} }
    else
      m = keys.map(&:to_s) - settings.map(&:key)
      raise ActiveRecord::RecordNotFound, "Couldn't find keys: #{m.join(',')}"
    end
  end

  def self.multi_find_or_initialize_by_key(keys)
    ret = where(key: keys).each { |t| keys.delete t.key } # rubocop:disable Rails/FindEach
    ret + keys.map { |k| new(key: k) }
  end
end
