# frozen_string_literal: true

class Store < ApplicationRecord
  belongs_to :city
  delegate :country, to: :city, allow_nil: true
  delegate :country_id, to: :city, allow_nil: true

  validates :name, presence: true
  validates :url, format: URI::DEFAULT_PARSER.make_regexp(%w[http https]), allow_blank: true

  default_scope { order(:city_id) }

  after_initialize :init_city
  after_destroy :cleanup

  def self.search(params)
    result = self

    result = result.where('name LIKE ?', "%#{params[:name]}%") if params[:name].present?

    result = result.joins(:city).where('city LIKE ?', "%#{params[:city]}%") if params[:city].present?

    result = result.joins(:city).where('cities.country_id = ?', params[:country_id]) if params[:country_id].present?

    result
  end

  def city_attributes=(city)
    self.city = City.find_or_init_city(city)
  end

  private

  def init_city
    self.city = City.new if city.nil?
  end

  # Destroy city if not longer needed
  def cleanup
    city.destroy if city.stores.count.zero?
  end
end
