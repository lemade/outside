# frozen_string_literal: true

require 'digest/sha1'
require 'openssl'

class Subscriber < ApplicationRecord
  include Rails.application.routes.url_helpers

  validates :email, presence: true, uniqueness: {case_sensitive: true}, format: /.+@.+/

  default_scope { order('created_at desc') }

  after_create :send_welcome_mail

  def self.ordered
    order('email')
  end

  TOKEN = Rails.application.secrets.subscriber_token

  # Get the subscribers signature
  def self.sign(url)
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('SHA1'), TOKEN, url)
  end

  # Verify the signature of the requested url with the subscribers signature
  def self.verify(url, signature)
    sign(url) == signature
  end

  # Verify the signature of the requested url with the subscribers signature
  def verify_url(url, sign)
    Subscriber.verify(url.sub(/(?:\?|&)signature=#{sign}/, ''), sign)
  end

  # Create signed_confirm_url to confirm the newsletter subscription
  def signed_confirm_url
    signed_url(confirm_subscriber_path(self))
  end

  # Create signed_unsubscribe_url to unsubscribe from the newsletter lis
  def signed_unsubscribe_url
    signed_url(unsubscribe_subscriber_path(self))
  end

  # Search parameters for the admin frontend index filter
  def self.search(params)
    result = self

    result = result.where('email LIKE ?', "%#{params[:email]}%") if params[:email].present?

    result = result.where(confirmed: params[:confirmed]) if params[:confirmed].present?

    result
  end

  # Get all confirmed
  def self.confirmed
    where(confirmed: true)
  end

  def human_confirmed
    I18n.t(confirmed?.to_s, scope: %i[labels subscriber confirmed])
  end

  private

  # Get the subscribers url and appended signature
  def signed_url(path)
    uri = URI("#{ENV.fetch('DEFAULT_URL_PROTOCOL')}://#{ENV.fetch('DEFAULT_URL_HOST')}#{path}")

    sign_url = uri.to_s
    uri.query = "signature=#{Subscriber.sign(sign_url)}"
    uri.to_s
  end

  def send_welcome_mail
    NewsletterMailer.delay.welcome_email(self)
  end
end
