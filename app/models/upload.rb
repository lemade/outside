# frozen_string_literal: true

class Upload < ApplicationRecord
  has_one_attached :dossier

  validates :dossier, content_type: %i[pdf], size: {less_than: 10.megabytes}
end
