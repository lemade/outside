# frozen_string_literal: true

require 'digest/sha1'
require 'securerandom'

class User < ApplicationRecord
  attr_accessor :password_old

  has_secure_password

  validates :username, presence: true, uniqueness: {case_sensitive: true}
  validates :password, presence: true, on: :create, length: {minimum: 6}
  validates :password, allow_blank: true, on: :update, length: {minimum: 6}
  validates :login_hash, uniqueness: {case_sensitive: true}, allow_nil: true

  validate :validate_old_password, if: :check_old_password_before_save?

  def self.ordered
    order('username')
  end

  def refresh_login_hash
    self.login_hash = unique_hash
  end

  def refresh_login_hash!
    refresh_login_hash
    save
  end

  def self.search(params)
    result = self

    result = result.where('username LIKE ?', "%#{params[:username]}%") if params[:username].present?

    result = result.where(admin: params[:admin]) if params[:admin].present?

    result
  end

  def check_old_password_before_save
    @check_old_password_before_save = true
  end

  def check_old_password_before_save?
    !!@check_old_password_before_save
  end

  def validate_old_password
    if password_digest_changed? &&
       BCrypt::Password.new(password_digest_was) != password_old
      errors.add(:password_old, I18n.t('messages.user.error.password'))
    end
  end

  private

  def unique_hash
    Digest::SHA1.hexdigest SecureRandom.uuid
  end
end
