lock '3.18.1'

set :application, 'outside'
set :repo_url, 'git@bitbucket.org:lemade/outside.git'

set :scm, :git
set :format, :pretty
set :log_level, :info
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{.env}

# Default value for linked_dirs is []
set :linked_dirs, %w{log public/packs storage tmp}

set :default_env, {
    'PATH' => '/package/host/localhost/ruby-3.2/bin/:$HOME/.gem/ruby/3.2/bin:$PATH'
}

SSHKit.config.command_map[:rake]  = 'bundle exec rake'
SSHKit.config.command_map[:rails] = 'bundle exec rails'

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do
  task :stop_rails_app do
    on roles(:app) do
      execute("/usr/bin/supervisorctl stop otb-#{fetch(:stage)}")
    end
  end

  desc 'Run rake npm:update'
  task :yarn_update do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install")
      end
    end
  end

  desc 'Run webpack'
  task :run_webpack do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && ./bin/webpack")
      end
    end
  end

  task :start_rails_app do
    on roles(:app) do
      execute("/usr/bin/supervisorctl start otb-#{fetch(:stage)}")
    end
  end

  before :deploy, 'deploy:stop_rails_app'
  before 'deploy:clobber_assets', 'deploy:compile_assets', 'deploy:yarn_update'
  after 'deploy:assets:precompile', 'deploy:run_webpack'
  after :deploy, 'deploy:start_rails_app'
end
