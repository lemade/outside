server 'phoenix.uberspace.de',
  user: 'luca',
  roles: %w{web app db}

set :branch, ENV['BRANCH'] || 'master'
set :rails_env, 'production'
set :deploy_to, '/home/luca/production'
