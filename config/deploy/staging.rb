server 'phoenix.uberspace.de',
  user: 'luca',
  roles: %w{web app db}

set :branch, ENV['BRANCH'] || 'develop'
set :rails_env, 'production'
set :deploy_to, '/home/luca/staging'
set :domain, 'staging.outside-mag.de'
