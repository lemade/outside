# frozen_string_literal: true

# Load the rails application
require_relative 'application'

# Initialize the rails application
Rails.application.initialize!

# Set the default host and port to be the same as Action Mailer.
Rails.application.default_url_options = Rails.application.config.action_mailer.default_url_options
