# frozen_string_literal: true

# The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s] # rubocop:disable Rails/FilePath
I18n.default_locale = :de
