# frozen_string_literal: true

Rails.application.routes.draw do
  # Public routes
  root to: 'public#index'

  resources :issues, only: [:index] do
    resources :posts, only: %i[index show]
  end

  resources :news, only: %i[index show]
  resources :papers, only: %i[index show]
  resources :subscribers, only: %i[index create new] do
    get :confirm, on: :member
    get :unsubscribe, on: :member
  end
  get 'stores(/:country)' => 'stores#index', as: :stores

  resource :about, only: [:show], controller: 'about'
  resource :search, only: [:show], controller: 'search'
  resource :impressum, only: [:show], controller: 'impressum'
  get '/datenschutz', to: 'dsgvo#show', as: :dsgvo

  # Admin routes
  get 'admin' => 'admin/issues#index', as: :admin

  namespace :admin do
    # Login
    get 'login' => 'login#form'
    post   'login' => 'login#login'
    delete 'login' => 'login#logout'

    # Users
    get    'profile' => 'users#profile'
    put    'profile' => 'users#update_profile'
    patch  'profile' => 'users#update_profile'

    resources :users, except: [:show]

    resources :issues, except: [:show] do
      resources :posts
    end

    resources :posts, only: [] do
      resource :gallery, only: %i[create destroy]
    end

    resources :papers
    resources :news
    resources :authors

    resources :newsletters do
      post :sendletter, on: :member
    end

    resources :subscribers, only: %i[index destroy]

    resources :attachment_sets, only: [] do
      resources :attachments, only: %i[create destroy]
    end

    # Settings
    resource  :about, only: %i[edit show update], controller: 'about'
    resource  :dsgvo, only: %i[edit show update], controller: 'dsgvo'
    resource  :impressum, only: %i[edit show update], controller: 'impressum'
    resources :links, except: [:show]
    resource :newsletter_info, only: %i[edit update], controller: 'newsletter_info'
    resource :sales_info, only: %i[edit update], controller: 'sales_info'
    resources :stores, except: [:show]
  end

  # Ajax routes
  namespace :ajax do
    post 'posts/sort' => 'posts#sort'

    resources :attachment_sets, only: [] do
      resources :attachments, only: %i[index create update destroy]
      post 'attachments/sort' => 'attachments#sort'
    end

    resources :uploads, only: %i[index create destroy]
  end
end
