const { environment } = require('@rails/webpacker')

const webpack = require('webpack')

environment.plugins.prepend(
    'Provide', new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
)

environment.loaders.append('expose', {
  test: /\.scss$/,
  use: [
    { loader: 'style-loader!css-loader!sass-loader' }
  ]
})

environment.loaders.append('expose', {
  test: /\.(ttf)(\?v=\d+\.\d+\.\d+)?$/,
  use: [
    { loader: 'file-loader', options: { name: '[name].[ext]', outputPath: 'fonts/' } }
  ]
})

const aliasConfig = {
    'jquery-ui': 'jquery-ui-dist/jquery-ui.js'
};

environment.config.set('resolve.alias', aliasConfig);

module.exports = environment