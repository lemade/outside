# frozen_string_literal: true

class InitOutside < ActiveRecord::Migration[5.0]
  def up
    create_table :users do |t|
      t.string   :username,        null: false
      t.boolean  :admin,           null: false, default: false
      t.string   :password_digest, null: false
      t.string   :login_hash,      null: true
      t.timestamps
    end

    add_index :users, :username, unique: true
    add_index :users, :login_hash, unique: true
  end

  def down
    drop_table :users
  end
end
