# frozen_string_literal: true

class CreateIssues < ActiveRecord::Migration[5.0]
  def up
    create_table :issues do |t|
      t.boolean  :published,     null: false, default: false
      t.string   :title,         null: false
      t.timestamps
    end

    add_index :issues, :title, unique: true
  end

  def down
    drop_table :issues
  end
end
