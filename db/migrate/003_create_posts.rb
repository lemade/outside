# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[5.0]
  def up
    create_table :posts do |t|
      t.integer  :issue_id,           null: false
      t.integer  :attachment_set_id,  null: false
      t.integer  :user_id,            null: false
      t.integer  :position,           null: false
      t.boolean  :published,          null: false, default: false
      t.string   :title,              null: false
      t.string   :subtitle
      t.text     :body
      t.timestamps
    end

    add_index :posts, :issue_id
    add_index :posts, :attachment_set_id, unique: true
    add_index :posts, :user_id
    add_index :posts, :title

    execute('ALTER TABLE posts ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_posts_body ON posts (body)')
  end

  def down
    drop_table :posts
  end
end
