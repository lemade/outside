# frozen_string_literal: true

class CreateGalleries < ActiveRecord::Migration[5.0]
  def up
    create_table :galleries do |t|
      t.integer :post_id,            null: false
      t.integer :attachment_set_id,  null: false
      t.timestamps
    end

    add_index :galleries, :post_id, unique: true
    add_index :galleries, :attachment_set_id, unique: true
  end

  def down
    drop_table :galleries
  end
end
