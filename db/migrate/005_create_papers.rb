# frozen_string_literal: true

class CreatePapers < ActiveRecord::Migration[5.0]
  def up
    create_table :papers do |t|
      t.integer  :attachment_set_id,  null: false
      t.integer  :user_id,            null: false
      t.boolean  :published,          null: false, default: false
      t.string   :title,              null: false, unique: false
      t.string   :subtitle
      t.string   :tag
      t.text     :body
      t.timestamps
    end

    add_index :papers, :attachment_set_id
    add_index :papers, :user_id
    add_index :papers, :title

    execute('ALTER TABLE papers ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_papers_body ON papers (body)')
  end

  def down
    drop_table :papers
  end
end
