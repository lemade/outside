# frozen_string_literal: true

class CreateAuthors < ActiveRecord::Migration[5.0]
  def up
    create_table :authors do |t|
      t.string   :name, null: false
      t.text     :info
      t.timestamps
    end

    add_index :authors, :name, unique: true

    create_table :authors_posts, id: false do |t|
      t.integer  :author_id
      t.integer  :post_id
    end

    add_index :authors_posts, %i[author_id post_id], unique: true

    create_table :authors_papers, id: false do |t|
      t.integer  :author_id
      t.integer  :paper_id
    end

    add_index :authors_papers, %i[author_id paper_id], unique: true
  end

  def down
    drop_table :authors
    drop_table :authors_posts
    drop_table :authors_papers
  end
end
