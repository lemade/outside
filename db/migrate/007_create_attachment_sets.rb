# frozen_string_literal: true

class CreateAttachmentSets < ActiveRecord::Migration[5.0]
  def up
    create_table :attachment_sets, &:timestamps
  end

  def down
    drop_table :attachment_sets
  end
end
