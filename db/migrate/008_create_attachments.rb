# frozen_string_literal: true

class CreateAttachments < ActiveRecord::Migration[5.0]
  def up
    create_table :attachments do |t|
      t.references        :attachable,   polymorphic: true
      t.integer           :position,     null: false
      t.integer           :width
      t.integer           :height
      t.string            :caption
      t.has_attached_file :image
    end
  end

  def down
    drop_table :attachments
  end
end
