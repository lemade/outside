# frozen_string_literal: true

class CreateUploads < ActiveRecord::Migration[5.0]
  def up
    create_table :uploads do |t|
      t.references         :attachable, polymorphic: true
      t.has_attached_file  :document
    end
  end

  def down
    drop_table :uploads
  end
end
