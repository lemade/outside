# frozen_string_literal: true

class CreateNewsletters < ActiveRecord::Migration[5.0]
  def up
    create_table :newsletters do |t|
      t.integer  :user_id,      null: false
      t.boolean  :published,    null: false, default: false
      t.string   :title,        null: false, unique: false
      t.text     :body
      t.timestamps
    end

    add_index :newsletters, :user_id
    add_index :newsletters, :title
  end

  def down
    drop_table :newsletters
  end
end
