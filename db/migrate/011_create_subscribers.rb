# frozen_string_literal: true

class CreateSubscribers < ActiveRecord::Migration[5.0]
  def up
    create_table :subscribers do |t|
      t.boolean  :confirmed,     null: false, default: false
      t.string   :name,          null: false
      t.string   :email,         null: false
      t.timestamps
    end

    add_index :subscribers, :name,  unique: true
    add_index :subscribers, :email, unique: true
  end

  def down
    drop_table :subscribers
  end
end
