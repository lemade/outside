# frozen_string_literal: true

class CreateSettings < ActiveRecord::Migration[5.0]
  def up
    create_table :settings do |t|
      t.string   :key
      t.text     :value
      t.timestamps
    end
  end

  def down
    drop_table :settings
  end
end
