# frozen_string_literal: true

class CreateStores < ActiveRecord::Migration[5.0]
  def up
    create_table :stores do |t|
      t.integer  :city_id,     null: false
      t.string   :name,        null: false
      t.string   :direction
      t.string   :url
      t.timestamps
    end

    add_index :stores, :city_id
    add_index :stores, :name

    create_table :countries do |t|
      t.string   :country,     null: false
    end

    add_index :countries, :country, unique: true

    create_table :cities do |t|
      t.integer  :country_id,  null: false
      t.string   :city,        null: false
    end

    add_index :cities, :country_id
    add_index :cities, :city
  end

  def down
    drop_table :stores
    drop_table :countries
    drop_table :cities
  end
end
