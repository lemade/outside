# frozen_string_literal: true

class CreateLinks < ActiveRecord::Migration[5.0]
  def up
    create_table :links do |t|
      t.string :name, null: false
      t.string :url,  null: false
    end
  end

  def down
    drop_table :links
  end
end
