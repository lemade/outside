# frozen_string_literal: true

class AddDefaultsToNotNull < ActiveRecord::Migration[5.0]
  def up
    change_column :attachments, :position, :integer, default: 0
    change_column :authors, :name, :string, default: ''
    change_column :cities, :city, :string, default: ''
    change_column :cities, :country_id, :integer, default: 0
    change_column :countries, :country, :string, default: ''
    change_column :galleries, :attachment_set_id, :integer, default: 0
    change_column :galleries, :post_id, :integer, default: 0
    change_column :issues, :title, :string, default: ''
    change_column :links, :name, :string, default: ''
    change_column :links, :url, :string, default: ''
    change_column :newsletters, :title, :string, default: ''
    change_column :newsletters, :user_id, :integer, default: 0
    change_column :papers, :attachment_set_id, :integer, default: 0
    change_column :papers, :title, :string, default: ''
    change_column :papers, :user_id, :integer, default: 0
    change_column :posts, :attachment_set_id, :integer, default: 0
    change_column :posts, :issue_id, :integer, default: 0
    change_column :posts, :title, :string, default: ''
    change_column :posts, :user_id, :integer, default: 0
    change_column :stores, :city_id, :integer, default: 0
    change_column :stores, :name, :string, default: ''
    change_column :subscribers, :email, :string, default: ''
    change_column :subscribers, :name, :string, default: ''
    change_column :users, :username, :string, default: ''
  end

  # because the default statement can't be removed
  def down
    change_column :attachments, :position, :integer, default: 0
    change_column :authors, :name, :string, default: ''
    change_column :cities, :city, :string, default: ''
    change_column :cities, :country_id, :integer, default: 0
    change_column :countries, :country, :string, default: ''
    change_column :galleries, :attachment_set_id, :integer, default: 0
    change_column :galleries, :post_id, :integer, default: 0
    change_column :issues, :title, :string, default: ''
    change_column :links, :name, :string, default: ''
    change_column :links, :url, :string, default: ''
    change_column :newsletters, :title, :string, default: ''
    change_column :newsletters, :user_id, :integer, default: 0
    change_column :papers, :attachment_set_id, :integer, default: 0
    change_column :papers, :title, :string, default: ''
    change_column :papers, :user_id, :integer, default: 0
    change_column :posts, :attachment_set_id, :integer, default: 0
    change_column :posts, :issue_id, :integer, default: 0
    change_column :posts, :title, :string, default: ''
    change_column :posts, :user_id, :integer, default: 0
    change_column :stores, :city_id, :integer, default: 0
    change_column :stores, :name, :string, default: ''
    change_column :subscribers, :email, :string, default: ''
    change_column :subscribers, :name, :string, default: ''
    change_column :users, :username, :string, default: ''
  end
end
