# frozen_string_literal: true

class AddNewsletterConsentToSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :subscribers, :nl_consent_text, :text
    add_column :subscribers, :nl_consent_date, :date
  end
end
