# frozen_string_literal: true

class ChangeNlConsentDateToTimestamp < ActiveRecord::Migration[5.0]
  def up
    change_table :subscribers do |t|
      t.change :nl_consent_date, :datetime
    end
  end

  def down
    change_table :subscribers do |t|
      t.change :nl_consent_date, :date
    end
  end
end
