# frozen_string_literal: true

class RemoveNameFromSubscribers < ActiveRecord::Migration[5.0]
  def up
    remove_column :subscribers, :name
  end

  def down
    add_column :subscribers, :name, :string
    add_index :subscribers, :name,  unique: true
  end
end
