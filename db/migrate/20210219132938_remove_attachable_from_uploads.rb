# frozen_string_literal: true

class RemoveAttachableFromUploads < ActiveRecord::Migration[5.0]
  def up
    remove_column :uploads, :attachable_id
    remove_column :uploads, :attachable_type
    remove_column :uploads, :document_content_type
    remove_column :uploads, :document_file_name
    remove_column :uploads, :document_file_size
    remove_column :uploads, :document_updated_at
  end

  def down
    add_column :uploads, :attachable_id, :integer
    add_column :uploads, :attachable_type, :string
    add_column :uploads, :document_content_type, type: string
    add_column :uploads, :document_file_name, type: string
    add_column :uploads, :document_file_size, type: integer
    add_column :uploads, :document_updated_at, type: datetime
  end
end
