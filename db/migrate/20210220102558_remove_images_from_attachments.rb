# frozen_string_literal: true

class RemoveImagesFromAttachments < ActiveRecord::Migration[5.0]
  def up
    remove_column :attachments, :image_content_type
    remove_column :attachments, :image_file_name
    remove_column :attachments, :image_file_size
    remove_column :attachments, :image_updated_at
  end

  def down
    add_column :attachments, :image_content_type, type: string
    add_column :attachments, :image_file_name, type: string
    add_column :attachments, :image_file_size, type: integer
    add_column :attachments, :image_updated_at, type: datetime
  end
end
