# frozen_string_literal: true

class CreateNews < ActiveRecord::Migration[5.0]
  def up
    create_table :news, if_not_exists: true do |t|
      t.integer  :attachment_set_id,  null: false
      t.integer  :user_id,            null: false
      t.boolean  :published,          null: false, default: false
      t.string   :title,              null: false, unique: false
      t.string   :subtitle
      t.string   :tag
      t.string   :info
      t.text     :body
      t.boolean  :on_stage, null: false, default: false
      t.datetime :on_stage_from
      t.datetime :on_stage_until
      t.timestamps
    end

    add_index :news, :attachment_set_id
    add_index :news, :user_id
    add_index :news, :title

    execute('ALTER TABLE news ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_news_body ON news (body)')
  end

  def down
    remove_index :news,[:attachment_sets_id, :user_id, :title]
    drop_table :news
  end
end
