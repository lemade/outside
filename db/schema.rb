# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_10_19_174029) do

  create_table "active_storage_attachments", charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", charset: "utf8mb4", collation: "utf8mb4_general_ci", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "attachment_sets", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attachments", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "attachable_id"
    t.string "attachable_type"
    t.integer "position", default: 0, null: false
    t.integer "width"
    t.integer "height"
    t.string "caption"
  end

  create_table "authors", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.text "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_authors_on_name", unique: true
  end

  create_table "authors_papers", id: false, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "author_id"
    t.integer "paper_id"
    t.index ["author_id", "paper_id"], name: "index_authors_papers_on_author_id_and_paper_id", unique: true
  end

  create_table "authors_posts", id: false, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "author_id"
    t.integer "post_id"
    t.index ["author_id", "post_id"], name: "index_authors_posts_on_author_id_and_post_id", unique: true
  end

  create_table "cities", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "country_id", default: 0, null: false
    t.string "city", default: "", null: false
    t.index ["city"], name: "index_cities_on_city"
    t.index ["country_id"], name: "index_cities_on_country_id"
  end

  create_table "countries", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "country", default: "", null: false
    t.index ["country"], name: "index_countries_on_country", unique: true
  end

  create_table "delayed_jobs", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "priority", default: 0
    t.integer "attempts", default: 0
    t.text "handler"
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "galleries", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "post_id", default: 0, null: false
    t.integer "attachment_set_id", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_set_id"], name: "index_galleries_on_attachment_set_id", unique: true
    t.index ["post_id"], name: "index_galleries_on_post_id", unique: true
  end

  create_table "issues", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.boolean "published", default: false, null: false
    t.string "title", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_issues_on_title", unique: true
  end

  create_table "links", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "url", default: "", null: false
  end

  create_table "news", id: :integer, charset: "utf8mb4", collation: "utf8mb4_general_ci", options: "ENGINE=MyISAM", force: :cascade do |t|
    t.integer "attachment_set_id", null: false
    t.integer "user_id", null: false
    t.boolean "published", default: false, null: false
    t.string "title", null: false
    t.string "subtitle"
    t.string "tag"
    t.string "info"
    t.text "body"
    t.boolean "on_stage", default: false, null: false
    t.datetime "on_stage_from"
    t.datetime "on_stage_until"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_set_id"], name: "index_news_on_attachment_set_id"
    t.index ["body"], name: "fulltext_news_body", type: :fulltext
    t.index ["title"], name: "index_news_on_title", length: 250
    t.index ["user_id"], name: "index_news_on_user_id"
  end

  create_table "newsletters", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.boolean "published", default: false, null: false
    t.string "title", default: "", null: false
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_newsletters_on_title"
    t.index ["user_id"], name: "index_newsletters_on_user_id"
  end

  create_table "papers", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", options: "ENGINE=MyISAM", force: :cascade do |t|
    t.integer "attachment_set_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.boolean "published", default: false, null: false
    t.string "title", default: "", null: false
    t.string "subtitle"
    t.string "tag"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_set_id"], name: "index_papers_on_attachment_set_id"
    t.index ["body"], name: "fulltext_papers_body", type: :fulltext
    t.index ["title"], name: "index_papers_on_title"
    t.index ["user_id"], name: "index_papers_on_user_id"
  end

  create_table "posts", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", options: "ENGINE=MyISAM", force: :cascade do |t|
    t.integer "issue_id", default: 0, null: false
    t.integer "attachment_set_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "position", null: false
    t.boolean "published", default: false, null: false
    t.string "title", default: "", null: false
    t.string "subtitle"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_set_id"], name: "index_posts_on_attachment_set_id", unique: true
    t.index ["body"], name: "fulltext_posts_body", type: :fulltext
    t.index ["issue_id"], name: "index_posts_on_issue_id"
    t.index ["title"], name: "index_posts_on_title"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "settings", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "key"
    t.text "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.integer "city_id", default: 0, null: false
    t.string "name", default: "", null: false
    t.string "direction"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_stores_on_city_id"
    t.index ["name"], name: "index_stores_on_name"
  end

  create_table "subscribers", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.boolean "confirmed", default: false, null: false
    t.string "email", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "nl_consent_text"
    t.datetime "nl_consent_date"
    t.index ["email"], name: "index_subscribers_on_email", unique: true
  end

  create_table "uploads", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
  end

  create_table "users", id: :integer, charset: "utf8mb3", collation: "utf8mb3_unicode_ci", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.boolean "admin", default: false, null: false
    t.string "password_digest", null: false
    t.string "login_hash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["login_hash"], name: "index_users_on_login_hash", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
end
