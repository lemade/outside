#  seeds.rb

I18n.locale = :de

I18n.t(:countries).each do |k, _|
  Country.create!(country: k)
end
