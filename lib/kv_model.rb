# frozen_string_literal: true

module KVModel
  extend ActiveSupport::Concern

  module ClassMethods
    def kv(class_name, *attribute_names)
      class_attribute(:kv_class)
      self.kv_class = class_name.to_s.constantize

      class_attribute(:kv_prefix)
      self.kv_prefix = "#{name.underscore}_"

      class_attribute(:kv_attrs)
      self.kv_attrs = attribute_names.map(&:to_s)

      kv_attrs.each { |attr| attr_accessor(attr) }

      define_method(:initialize) do
        keys = kv_attrs.map { |attr| kv_prefix + attr }

        kv_class.get(*keys).each do |k, v|
          send("#{k[kv_prefix.length..-1]}=", v)
        end
      end

      define_method(:attributes=) do |values|
        kv_attrs.each do |key|
          send("#{key}=", values[key]) if values.key?(key)
        end
      end

      define_method(:update_attributes) do |values|
        if values
          self.attributes = values.stringify_keys
          save
        else
          false
        end
      end

      define_method(:save) do
        hsh = {}
        kv_attrs.each { |attr| hsh[kv_prefix + attr] = send(attr) }
        kv_class.set(hsh)
      end
    end
  end
end
