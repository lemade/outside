# frozen_string_literal: true

FactoryBot.define do
  factory :attachment_set do
    trait :with_attachments do
      after(:create) do |set|
        build_list(:attachment, 3, attachable_id: set.id)
      end
    end
  end
end
