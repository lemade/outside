# frozen_string_literal: true

FactoryBot.define do
  factory :author do
    name { SecureRandom.uuid }

    trait :with_posts do
      after_create do |author|
        build_list(:post, 3, author: author)
      end
    end

    trait :with_papers do
      after_create do |author|
        build_list(:paper, 3, author: author)
      end
    end
  end
end
