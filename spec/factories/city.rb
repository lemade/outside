# frozen_string_literal: true

FactoryBot.define do
  factory :city do
    country
    city { FFaker::Address.city }
  end
end
