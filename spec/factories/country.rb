# frozen_string_literal: true

FactoryBot.define do
  factory :country do
    country { FFaker::Address.country_code }
  end
end
