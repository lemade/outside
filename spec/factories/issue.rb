# frozen_string_literal: true

FactoryBot.define do
  factory :issue do
    title { SecureRandom.uuid }

    trait :published do
      published { true }
    end
  end
end
