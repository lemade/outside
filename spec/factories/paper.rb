# frozen_string_literal: true

FactoryBot.define do
  factory :paper do
    attachment_set
    user

    title { SecureRandom.uuid }
    subtitle { SecureRandom.uuid }
    tag { SecureRandom.uuid }

    trait :published do
      published { true }
    end
  end
end
