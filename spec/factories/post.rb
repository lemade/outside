# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    attachment_set
    issue
    user

    title { SecureRandom.uuid }
    subtitle { SecureRandom.uuid }

    trait :published do
      published { true }
    end

    trait :with_gallery do
      after(:create) do |post|
        create(:gallery, post_id: post.id)
      end
    end
  end
end
