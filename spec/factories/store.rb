# frozen_string_literal: true

FactoryBot.define do
  factory :store do
    city
    name { FFaker::HipsterIpsum.word }
    direction { FFaker::Address.street_address }
    url { FFaker::Internet.http_url }
  end
end
