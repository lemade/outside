# frozen_string_literal: true

FactoryBot.define do
  factory :subscriber do
    email { FFaker::Internet.email }

    trait :confirmed do
      confirmed { true }
    end
  end
end
