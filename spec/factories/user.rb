# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { FFaker::Name.name }
    password { 'secret123' }
    password_confirmation { password }

    trait :admin do
      admin { true }
    end
  end
end
