# frozen_string_literal: true

require 'rails_helper'

feature 'Update dsgvo' do
  let(:password) { 'secret123' }
  let(:new_title) { FFaker::Name.name }

  before do
    login_as_admin
    visit edit_admin_dsgvo_path
  end

  it 'saves changes and shows success notice' do
    within('form') do
      fill_in '_dsgvo_title', with: new_title
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_dsgvo_path)
    expect(page).to have_content(new_title)
    expect(page).to have_content('Änderungen wurden gespeichert')
  end
end
