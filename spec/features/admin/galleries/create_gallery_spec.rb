# frozen_string_literal: true

require 'rails_helper'

feature 'Create gallery' do
  let(:password) { 'secret123' }
  let(:post) { create(:post) }

  before do
    login_as_admin
    visit admin_issue_post_path(post.issue, post)
  end

  it 'saves gallery and shows success notice' do
    within('div.show-actions', match: :first) do
      click_button('Gallerie hinzufügen')
    end

    expect(page).to have_current_path(admin_issue_post_path(post.issue, post))
    expect(page).to have_content('Gallerie wurde hinzugefügt.')
    expect(page).to have_content('Gallerie entfernen')
    expect(page).to have_selector('input#attachment_picture')
  end
end
