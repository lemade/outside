# frozen_string_literal: true

require 'rails_helper'

feature 'Delete gallery' do
  let(:password) { 'secret123' }
  let(:post) { create(:post, :with_gallery) }

  before do
    login_as_admin
    visit admin_issue_post_path(post.issue, post)
  end

  it 'deletes gallery and shows success notice' do
    within('div.show-actions', match: :first) do
      click_button('Gallerie entfernen')
    end

    expect(page).to have_current_path(admin_issue_post_path(post.issue, post))
    expect(page).to have_content('Gallerie wurde gelöscht.')
    expect(page).to have_content('Gallerie hinzufügen')
    expect(page).to have_no_selector('input#attachment_image')
  end
end
