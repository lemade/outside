# frozen_string_literal: true

require 'rails_helper'

feature 'Create paper' do
  let(:password) { 'secret123' }
  let(:title) { FFaker::Book.title }
  let(:subtitle) { FFaker::Book.title }
  let(:author) { create(:author) }
  let(:markdown_body) { '**Something** is bold here.' }

  before do
    login_as_admin
    visit new_admin_paper_path
  end

  it 'saves paper and shows success notice' do
    within('form#new_paper') do
      fill_in 'paper_title', with: title
      fill_in 'paper_subtitle', with: subtitle
      fill_in 'paper_body', with: markdown_body
      fill_in 'authors', with: author.name
      check 'paper_published'
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_paper_path(Paper.last))
    expect(page).to have_content("\"#{title}\" wurde gespeichert.")
    expect(page).to have_content(author.name)
    expect(page).to have_css('strong')
    expect(page).to have_content(author.info)
  end
end
