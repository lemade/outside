# frozen_string_literal: true

require 'rails_helper'

feature 'Create post' do
  let(:password) { 'secret123' }
  let(:issue) { create(:issue) }
  let(:author) { create(:author) }
  let(:title) { FFaker::Book.title }
  let(:subtitle) { FFaker::Book.title }
  let(:markdown_body) { '**Something** is bold here.' }

  before do
    login_as_admin
    visit new_admin_issue_post_path(issue)
  end

  it 'saves post and shows success notice' do
    within('form#new_post') do
      fill_in 'post_title', with: title
      fill_in 'post_subtitle', with: subtitle
      fill_in 'post_body', with: markdown_body
      fill_in 'authors', with: author.name
      check 'post_published'
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_issue_post_path(issue, Post.last))
    expect(page).to have_content("\"#{title}\" wurde gespeichert.")
    expect(page).to have_content(author.name)
    expect(page).to have_css('strong')
    expect(page).to have_content(author.info)
  end
end
