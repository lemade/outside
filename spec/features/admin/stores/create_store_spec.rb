# frozen_string_literal: true

require 'rails_helper'

feature 'Create store' do
  let(:password) { 'secret123' }
  let(:store_name) { FFaker::Name.name }

  before do
    login_as_admin
    visit new_admin_store_path
  end

  xit 'saves store and shows success notice' do
    within('form#new_store') do
      fill_in 'store_name', with: store_name
      fill_in 'store_direction', with: FFaker::Address.street_address
      fill_in 'store_city_attributes_city', with: 'Berlin'
      first('#store_city_attributes_country_id', visible: false).set('DE')
      fill_in 'store_url', with: FFaker::Internet.http_url
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_stores_path)
    expect(page).to have_content(store_name)
  end
end
