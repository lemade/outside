# frozen_string_literal: true

require 'rails_helper'

feature 'Delete store' do
  let(:password) { 'secret123' }

  before do
    login_as_admin
    create_list(:store, 3)
    visit admin_stores_path
  end

  it 'deletes store', js: true do
    within('table.index-table') do
      find(:css, 'i.fa-trash', match: :first).click
    end

    expect(page).to have_content('wurde gelöscht.')
    expect(page).to have_selector('tbody > tr', count: 2)
  end
end
