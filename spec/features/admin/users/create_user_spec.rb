# frozen_string_literal: true

require 'rails_helper'

feature 'Create a user' do
  let(:password) { 'secret123' }
  let(:username) { FFaker::Name.name }

  before do
    login_as_admin
    visit new_admin_user_path
  end

  it 'saves an admin user and shows success notice' do
    within('form#new_user') do
      fill_in 'user_username', with: username
      fill_in 'user_password', with: password
      fill_in 'user_password_confirmation', with: password
      check 'user_admin'
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_users_path)
    expect(page).to have_content(username)
    expect(page).to have_content('Administrator#in')
    expect(page).to have_content("Benutzer#in \"#{username}\" gespeichert.")
  end

  it 'saves an editor and shows success notice' do
    within('form#new_user') do
      fill_in 'user_username', with: username
      fill_in 'user_password', with: password
      fill_in 'user_password_confirmation', with: password
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_users_path)
    expect(page).to have_content(username)
    expect(page).to have_content('Redakteur#in')
    expect(page).to have_content("Benutzer#in \"#{username}\" gespeichert.")
  end

  context 'with invalid' do
    it 'will show error notice' do
      within('form#new_user') do
        fill_in 'user_username', with: ''
        click_button 'Speichern'
      end

      expect(page).to have_content('Konnte Benutzer#in nicht speichern.')
    end
  end
end
