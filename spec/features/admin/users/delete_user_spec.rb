# frozen_string_literal: true

require 'rails_helper'

feature 'Deleting a user' do
  let(:password) { 'secret123' }

  before do
    login_as_admin
    visit admin_users_path
  end

  it 'is not possible to delete themselves', js: true do
    within('table.index-table') do
      find(:css, 'i.fa-trash', match: :first).click
    end

    expect(page).to have_current_path(admin_users_path)
    expect(page).to have_content('User kann sich nicht selbst löschen.')
  end

  context 'deleting another user', js: true do
    let!(:other_user) { create(:user) }

    it 'deletes the user and shows success notice' do
      within('div.index-filter') do
        fill_in 'username', with: other_user.username
        click_on 'Filtern'
      end

      within('table.index-table') do
        find(:css, 'i.fa-trash', match: :first).click
      end

      expect(page).to have_current_path(admin_users_path)
      expect(page).to have_content("Benutzer#in \"#{other_user.username}\" wurde gelöscht.")
    end
  end
end
