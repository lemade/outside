# frozen_string_literal: true

require 'rails_helper'

feature 'Login' do
  before { visit admin_login_path }

  context 'with account' do
    let(:user) { create(:user) }
    let(:password) { 'secret123' }

    before do
      fill_in 'username', with: user.username
      fill_in 'password', with: password
      click_on 'Login'
    end

    it 'logs in the user' do
      expect(page).to have_current_path(admin_path)
      expect(page).to have_content('Hello Again!')
      expect(page).to_not have_content('Einstellungen')
    end

    context 'as an admin user' do
      let(:user) { create(:user, :admin) }

      it 'logs in the admin user' do
        expect(page).to have_current_path(admin_path)
        expect(page).to have_content('Hello Again!')
        expect(page).to have_content('Einstellungen')
      end
    end
  end

  context 'with invalid' do
    it 'does not log in the user' do
      fill_in 'username', with: FFaker::Name.name
      fill_in 'password', with: FFaker::Internet.password
      click_on 'Login'

      expect(page).to have_current_path(admin_login_path)
      expect(page).to have_content('Password oder Name ist falsch.')
    end
  end
end
