# frozen_string_literal: true

require 'rails_helper'

feature 'Update a user' do
  let(:password) { 'secret123' }
  let(:user) { create(:user) }
  let(:new_username) { FFaker::Name.name }
  let(:new_password) { 'password123' }

  before do
    login_as_admin
    visit edit_admin_user_path(user)
  end

  it 'updates the user and shows success notice' do
    within('form.edit_user') do
      fill_in 'user_username', with: new_username
      fill_in 'user_password', with: new_password
      fill_in 'user_password_confirmation', with: new_password
      click_on 'Speichern'
    end

    expect(page).to have_current_path(admin_users_path)
    expect(page).to have_content('Änderungen wurden gespeichert.')
  end

  context 'without changing password' do
    it 'updates the user and shows success notice' do
      within('form.edit_user') do
        fill_in 'user_username', with: new_username
        click_on 'Speichern'
      end

      expect(page).to have_current_path(admin_users_path)
      expect(page).to have_content('Änderungen wurden gespeichert.')
    end
  end

  context 'with invalid' do
    it 'will show error notice' do
      within('form.edit_user') do
        fill_in 'user_username', with: ''
        click_button 'Speichern'
      end

      expect(page).to have_content('Änderungen konnten nicht gespeichert werden.')
    end
  end
end
