# frozen_string_literal: true

require 'rails_helper'

feature 'Accept cookies:' do
  before do
    visit root_path
  end

  context 'without cookies allowed', js: true do
    it 'renders a cookie bar' do
      expect(page).to have_css('#cookies-bar')
    end

    context 'accepting cookies' do
      it 'does not show the cookies bar' do
        find('#read-cookie-hint').click

        expect(page).to have_no_css('#cookies-bar')
      end
    end
  end
end
