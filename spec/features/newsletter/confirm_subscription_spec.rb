# frozen_string_literal: true

require 'rails_helper'

feature 'Confirming a subscription' do
  let(:subscriber) { create(:subscriber) }
  let(:confirm_url) { subscriber.signed_confirm_url }
  let(:signature) { URI.parse(confirm_url).query.sub(/^signature?=?/, '') }

  before do
    visit confirm_subscriber_url(subscriber, signature: signature)
  end

  context 'with valid url' do
    it 'confirmes the subscriber' do
      expect(page).to have_content('Du hast den Newsletter erfolgreich abonniert.')
    end
  end

  context 'with invalid url' do
    let(:signature) { 'invalid' }

    it 'shows an error message' do
      expect(page).to have_content('Uups! Das hat nicht geklappt.')
    end
  end
end
