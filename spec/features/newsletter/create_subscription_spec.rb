# frozen_string_literal: true

require 'rails_helper'

feature 'Create a subscription' do
  let(:email) { FFaker::Internet.email }

  before do
    visit new_subscriber_path

    fill_in :subscriber_email, with: email
    allow_any_instance_of(ActionController::Base).to receive(:verify_rucaptcha?).and_return(true)
    click_on 'Speichern'
  end

  context 'with valid' do
    it 'shows a success notice' do
      expect(page).to have_content('Du hast eine Mail erhalten. Bitte bestätige den Link in der Mail.')
    end

    it 'delayes a welcome email job' do
      expect(Delayed::Job.last.handler).to have_content('welcome_email')
      expect(Delayed::Job.last.handler).to have_content(email)
    end
  end

  context 'with_invalid' do
    let(:email) { 'invalid' }

    it 'shows an error message' do
      expect(page).to have_content('Uups! Das hat nicht geklappt.')
    end
  end
end
