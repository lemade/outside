# frozen_string_literal: true

require 'rails_helper'

feature 'Deleting a subscription' do
  let(:subscriber) { create(:subscriber, :confirmed) }
  let(:unsubscribe_url) { subscriber.signed_unsubscribe_url }
  let(:signature) { URI.parse(unsubscribe_url).query.sub(/^signature?=?/, '') }

  before do
    visit unsubscribe_subscriber_url(subscriber, signature: signature)
  end

  context 'with valid url' do
    it 'deletes the subscriber' do
      expect(page).to have_content('Du hast dich aus dem outside the box Newsletter ausgetragen.')
    end
  end

  context 'with invalid url' do
    let(:signature) { 'invalid' }

    it 'shows an error message' do
      expect(page).to have_content('Uups! Das hat nicht geklappt.')
    end
  end
end
