# frozen_string_literal: true

require 'spec_helper'

describe Author, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name) }
end
