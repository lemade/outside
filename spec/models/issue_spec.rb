# frozen_string_literal: true

require 'spec_helper'

describe Issue, type: :model do
  it { is_expected.to have_many(:posts) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_uniqueness_of(:title) }

  it { is_expected.to validate_content_type_of(:picture).allowing('image/png', 'image/gif', 'image/jpeg') }
  it { is_expected.to validate_size_of(:picture).less_than(5.megabytes) }
end
