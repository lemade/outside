# frozen_string_literal: true

require 'spec_helper'

describe Paper, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_and_belong_to_many(:authors) }
  # it { is_expected.to belong_to(:attachment_set)}

  it { is_expected.to validate_presence_of(:title) }
end
