# frozen_string_literal: true

require 'spec_helper'

describe Post, type: :model do
  it { is_expected.to belong_to(:issue) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:attachments).through(:attachment_set) }

  it { is_expected.to validate_presence_of(:title) }
end
