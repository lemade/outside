# frozen_string_literal: true

require 'spec_helper'

describe User, type: :model do
  subject { build(:user) }

  it { is_expected.to validate_presence_of(:username) }
  it { is_expected.to validate_uniqueness_of(:username) }

  it { is_expected.to have_secure_password }
end
