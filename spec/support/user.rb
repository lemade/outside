# frozen_string_literal: true

def login_as_admin
  user = FactoryBot.create(:user, :admin)

  visit admin_login_path
  fill_in 'username', with: user.username
  fill_in 'password', with: password
  click_on 'Login'
end
